package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.*;
import cn.com.dollar.autoTest.enumeration.ActionType;
import cn.com.dollar.autoTest.service.implement.AutoTestServiceImp;
import cn.com.dollar.autoTest.service.interfaces.AutoTestService;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.engine.act.main.model.ActivityMainInfo;
import cn.com.dollar.engine.act.main.service.IActQueryService;
import cn.com.dollar.engine.statistics.model.type.DataDimension;
import cn.com.dollar.engine.statistics.model.type.PageActionType;
import cn.com.dollar.engine.statistics.model.type.PlatformBrowserType;

import cn.com.dollar.engine.statistics.model.type.StatisticsNumType;
import cn.com.dollar.engine.trophy.main.model.TrophyInfo;
import cn.com.dollar.engine.trophy.main.service.ITrophyQueryService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ZZJ on 2017-5-2.
 */
@RestController
@RequestMapping(value = "statistics")
public class DataStatisticsTestController {
    private ResponseUtil responseUtil=new ResponseUtil();
    @Autowired
    IActQueryService actQueryService;
    @Autowired
    ITrophyQueryService trophyQueryService;
    @RequestMapping(value = "pageViewTest")
    public Map pageViewTest(){
        CredentialType type=CredentialType.MOBILE;

        JSONArray msgArray=new JSONArray();
        //并发抽奖
        //设置并发数
        int concurrent=200;
        AutoTestService autoTestService=new AutoTestServiceImp();
        JSONObject resJSON=autoTestService.ConcurrentDrawOrderly(concurrent,Configs.engineBaseURL+Configs.drawURL,Configs.pageViewActId,type);
        JSONArray authKeys=resJSON.getJSONArray("authkeys");
        //成功挤进服务器并返回200的请求数
        Integer successCount=resJSON.getInteger("count");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //TODO 目前分享数据是通过分享回调接口产生的，以后可能放到埋点事件上传接口。
        //生成分享数据
        AtomicInteger shareSum = new AtomicInteger(0);
        ExecutorService sharePool = Executors.newCachedThreadPool();
        for(int i=0;i<authKeys.size();i++){
            ShareCaseDto shareDto=new ShareCaseDto();
            shareDto.setActivityId(Configs.pageViewActId.toString());
            shareDto.setAuthKey(authKeys.getString(i));
            shareDto.setType(type);
            //朋友圈和会话分享各一半人数
            if(i<(authKeys.size()/2)){
                shareDto.setActionType(ActionType.WETCH_FRIEND_CIRCLE_SHARE);
            }else{
                shareDto.setActionType(ActionType.WETCH_TALKING_SHARE);
            }

            sharePool.execute(new Runnable() {
                @Override
                public void run() {
                    JSONObject res= HttpUtil.post(Configs.engineBaseURL+Configs.shareURL,shareDto,null);
                    if(res.getIntValue("code")==200){
                        shareSum.getAndAdd(1);
                    }
                }
            });
        }
        sharePool.shutdown();
        System.out.println("shutdown()：启动一次顺序关闭，执行以前提交的任务，但不接受新任务。");
        while(true){
            if(sharePool.isTerminated()){
                System.out.println("所有的子线程都结束了！");
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //并发提交埋点数据
        AtomicInteger num = new AtomicInteger(0);
        ExecutorService pool = Executors.newCachedThreadPool();
        for(int i=0;i<authKeys.size();i++){
            String authkey=authKeys.getString(i);
            pool.execute(new Runnable() {
                @Override
                public void run() {
                    JSONObject res=pageViewRequest(authkey);
                    if(res.getInteger("code")==200){
                        num.getAndAdd(1);
                    }
                }
            });
        }
        pool.shutdown();
        System.out.println("shutdown()：启动一次顺序关闭，执行以前提交的任务，但不接受新任务。");
        while(true){
            if(pool.isTerminated()){
                System.out.println("所有的子线程都结束了！");
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //校验统计数据
        JSONObject res=HttpUtil.get(Configs.portalBaseURL+Configs.queryActivityBaseInfoURL+Configs.getPageViewAlias,null);
        if(res.getInteger("code")==200){
            //历史在线人数峰值
            int maxOnlineUserCount=res.getIntValue("maxOnlineUserCount");
            if(maxOnlineUserCount!=num.intValue()){
                String msg="历史在线峰值查询结果："+maxOnlineUserCount+"，理论值应该是"+num;
                msgArray.add(msg);
            }

            //总的奖品价值
            double trophyTotalWorth= res.getDoubleValue("trophyTotalWorth");
            //剩余奖品价值
            double trophyLeftWorth=res.getDoubleValue("trophyLeftWorth");
            List<TrophyInfo> trophyList=trophyQueryService.queryTrophyList(Configs.pageViewActId,1,1).content;
            double singleWorth=trophyList.get(0).trophyWorth.doubleValue();
            double leftWorth=trophyTotalWorth-successCount*singleWorth;
            if(leftWorth!=trophyLeftWorth){
                String msg="剩余奖品价值有误，查询结果："+trophyLeftWorth+",预期结果："+leftWorth;
                msgArray.add(msg);
            }
            if(trophyLeftWorth<0){
                String msg2="剩余奖品价值为负："+trophyLeftWorth;
                msgArray.add(msg2);
            }
            //已发放金额
//            Double issued=trophyTotalWorth-trophyLeftWorth;

            JSONObject activityMainInfo=res.getJSONObject("activityMainInfo");
            //活动中奖次数
            Integer lotteryCount = activityMainInfo.getInteger("lotteryCount");
            if(lotteryCount!=null){
                if(!lotteryCount.equals(successCount)){
                    String msg="中奖次数有误，查询结果："+lotteryCount+"，预期结果："+successCount;
                    msgArray.add(msg);
                }
            }else{
                String msg="中奖次数返回null";
                msgArray.add(msg);
            }
            //中奖的人数
            Integer lotteryUserCount=activityMainInfo.getInteger(" lotteryUserCount");
            if(lotteryUserCount!=null){
                if(!lotteryUserCount.equals(successCount)){
                    String msg="中奖人数有误，查询结果："+lotteryUserCount+"，预期结果："+successCount;
                    msgArray.add(msg);
                }
            }else{
                String msg="中奖人数返回null";
                msgArray.add(msg);
            }
            //截止参与的次数（PV）,就是打开首页页面的次数
            int participateCount=activityMainInfo.getIntValue("participateCount");
            if(participateCount!=num.intValue()){
                String msg="截止目前的参与次数（PV）有误，查询结果："+participateCount+"，预期结果："+num;
                msgArray.add(msg);
            }
            //截止参与的人数（UV）,就是打开首页页面的人数
            int participateUserCount=activityMainInfo.getIntValue("participateUserCount");
            if(participateUserCount!=num.intValue()){
                String msg="截止目前的参与人数（UV）有误，查询结果："+participateUserCount+"，预期结果："+num;
                msgArray.add(msg);
            }
        }
        ActivityMainInfo actMsg=actQueryService.queryActivityMainById(Configs.pageViewActId);
        List<UserBehaviorTotalDataDto> param=UserBehaviorTotalDataDto.getList(actMsg.startDate,actMsg.endDate);
        res=HttpUtil.put(Configs.portalBaseURL+Configs.queryUserBehaviorTotalDataRUL,null,param);

        if(res.getInteger("code")==200){
            userBehaviorTotalDataCheck(res,successCount,shareSum.intValue(),num.intValue(),msgArray);
        }

        //画图相关接口数据检查
        //queryActivityTotalSta接口数据检查
        List<ActivityTotalStaQueryDto> dtoList=getActivityTotalStaQueryDto();
        res=HttpUtil.put(Configs.portalBaseURL+Configs.queryActivityTotalStaURL,null,dtoList);
        if(res.getInteger("code")==200){
            activityTotalStaCheck(res,msgArray,num.intValue());
        }else{
            String msg="数据接口:"+Configs.queryActivityTotalStaURL+"，出错："+res.getString("msg");
            msgArray.add(msg);
        }

        //queryUserBehaviorData接口数据检查
        queryUserBehaviorDataCheck(msgArray);

        //shareDegree接口数据检查
        shareDegreeDataCheck(msgArray,authKeys);

        //distribution接口数据检查
        distributionDataCheck(msgArray,successCount);

        if(msgArray.size()>0){
            return responseUtil.sendResult(false,msgArray);
        }
        return responseUtil.sendResult(true,"");
    }

    public JSONObject pageViewRequest(String authkey){
//        String uuid=UUID.randomUUID().toString();
        Long totalTime=Configs.totalTime;
        PageViewDto pageViewDto=new PageViewDto();
        pageViewDto.setOpenId(authkey);
        pageViewDto.setActivityId(Configs.getPageViewAlias);
        pageViewDto.setBrowserId(authkey);
        pageViewDto.setStayTime(totalTime);
        pageViewDto.setPlatformBrowserType(PlatformBrowserType.WECHAT);

        List<PageActionType> typeList=new ArrayList<>();
        typeList.add(PageActionType.CLICK_AD);
        //分享的等英杰添加修改
//        typeList.add(PageActionType.SHARE);

        pageViewDto.setActions(getActionDtoList(typeList,totalTime.intValue()));
        String jsonObject=JSONObject.toJSONString(pageViewDto);
        System.out.println(jsonObject);

        return  HttpUtil.post(Configs.engineBaseURL+Configs.pageViewURL,pageViewDto,null);
    }

    public List<ActionDto> getActionDtoList(List<PageActionType> list,int totalTime){
        List<ActionDto> result=new ArrayList<>();
        long now=System.currentTimeMillis();
        Date date=new Date(now);

        //打开页面
        ActionDto dto=new ActionDto();
        dto.setAction(PageActionType.OPEN_HOME_PAGE);
        dto.setTime(date);
        result.add(dto);

        //上线事件
        ActionDto dto3=new ActionDto();
        dto3.setAction(PageActionType.UP_LINE);
        dto3.setTime(date);
        result.add(dto3);

        //其他事件
        int limit=totalTime-1;
        Random rand = new Random();
        int temp=0;
        for(PageActionType actionType:list){
            int i=rand.nextInt(limit)+1;

            while(temp==i){
                i=rand.nextInt(limit)+1;
            }
            ActionDto actionDto=new ActionDto();
            actionDto.setAction(actionType);
            actionDto.setTime(new Date(now+1000L*i));
            result.add(actionDto);

            temp=i;
        }

        //关闭页面
        ActionDto dto2=new ActionDto();
        dto2.setAction(PageActionType.DOWN_LINE);
        dto2.setTime(new Date(now+1000L*totalTime));
        result.add(dto2);
        return result;
    }

    private void userBehaviorTotalDataCheck(JSONObject res,int successCount,int shareSum,int num,JSONArray msgArray){
        JSONArray array=res.getJSONArray("dataArray");
        for(int i=0;i<array.size();i++){
            JSONObject o=array.getJSONObject(i);
            switch (o.getString("datatype")){
                case "SHARE_COUNT":{
                    //截止到当前的分享次数
                    int shareCount=o.getInteger("numCount");
                    if(shareCount!=shareSum){
                        String msg="截止到当前的分享次数有误，查询结果："+shareCount+",预期结果："+shareSum;
                        msgArray.add(msg);
                    }
                    break;
                }
                case "CLICK_AD_COUNT":{
                    //广告点击次数
                    int clickAdCount=o.getIntValue("numCount");
                    if(clickAdCount!=num){
                        String msg="广告点击次数有误，查询结果："+clickAdCount+"，预期结果："+num;
                        msgArray.add(msg);
                    }
                    break;
                }
                case "CLICK_AD_USER_COUNT":{
                    //广告点击人数
                    int clickAdUserCount=o.getIntValue("numCount");
                    if(clickAdUserCount!=num){
                        String msg="广告点击人数有误，查询结果："+clickAdUserCount+"，预期结果："+num;
                        msgArray.add(msg);
                    }
                    break;
                }
                case "ONLINE_LONGEST_TIME":{
                    //最长在线时间
                    int onlineLongestTime=o.getIntValue("numCount");
                    int time=Configs.totalTime.intValue();
                    if(onlineLongestTime!=time){
                        String msg="最长在线时间有误，查询结果："+onlineLongestTime+",预期结果："+time;
                        msgArray.add(msg);
                    }
                    break;
                }
                case "ONLINE_SUM_TIME":{
                    //累计在线时间
                    int onlineSumTime=o.getIntValue("numCount");
                    int totalTime=num*Configs.totalTime.intValue();
                    if(onlineSumTime!=totalTime){
                        String msg="累计在线时间有误，查询结果："+onlineSumTime+",预期结果："+totalTime;
                        msgArray.add(msg);
                    }
                    break;
                }
                case "DRAW_COUNT":{
                    //抽奖次数
                    int drawCount=o.getIntValue("numCount");
                    if(drawCount!=successCount){
                        String msg="抽奖次数有误，查询结果："+drawCount+"，预期结果："+successCount;
                        msgArray.add(msg);
                    }
                    break;
                }
                case "DRAW_USER_COUNT":{
                    //抽奖人数
                    int drawUserCount=o.getIntValue("numCount");
                    if(drawUserCount!=successCount){
                        String msg="抽奖人数有误，查询结果："+drawUserCount+",预期结果："+successCount;
                        msgArray.add(msg);
                    }
                    break;
                }
                default: break;
            }
        }

    }

    private List<ActivityTotalStaQueryDto> getActivityTotalStaQueryDto(){
        List<ActivityTotalStaQueryDto> list=new ArrayList<>();
        StatisticsNumType types[]={StatisticsNumType.ONLINE_LONGEST_TIME,StatisticsNumType.ONLINE_SUM_TIME,
                StatisticsNumType.AVER_ONLINE_TIME,StatisticsNumType.ONLINE_TIME_1_TO_20S,StatisticsNumType.ONLINE_TIME_21_TO_40S,
                StatisticsNumType.ONLINE_TIME_41_TO_60S,StatisticsNumType.ONLINE_TIME_61_TO_MAX};

        for(StatisticsNumType type:types){
            ActivityTotalStaQueryDto dto=new ActivityTotalStaQueryDto();
            dto.setActId(Configs.getPageViewAlias);
            dto.setDataType(type);

            list.add(dto);
        }
        return list;
    }

    private void activityTotalStaCheck(JSONObject res,JSONArray msgArray,int num){
        JSONArray array=res.getJSONArray("dataArray");
        int time=Configs.totalTime.intValue();
        for(int i=0;i<array.size();i++){
            JSONObject o=array.getJSONObject(i);
            switch (o.getString("dataType")){
                case "ONLINE_LONGEST_TIME":{
                    int numCount=o.getIntValue("numCount");
                    if(numCount!=time){
                        String msg="画图接口queryActivityTotalSta的最长在线时间有误，查询结果："+numCount+",预期结果："+time;
                        msgArray.add(msg);
                    }
                    break;
                }

                case "ONLINE_SUM_TIME":{
                    int onlineSumTime=o.getIntValue("numCount");
                    int totalTime=num*time;
                    if(onlineSumTime!=totalTime){
                        String msg="画图接口queryActivityTotalSta的累计在线时间有误，查询结果："+onlineSumTime+",预期结果："+totalTime;
                        msgArray.add(msg);
                    }
                    break;
                }

                case "AVER_ONLINE_TIME":{
                    int numCount=o.getIntValue("numCount");
                    if(numCount!=time){
                        String msg="画图接口queryActivityTotalSta的平均在线时间有误，查询结果："+numCount+",预期结果："+time;
                        msgArray.add(msg);
                    }
                    break;
                }

                case "ONLINE_TIME_1_TO_20S":{
                    if(o.getIntValue("numCount")!=0){
                        String msg="画图接口queryActivityTotalSta的在线时间(1-20S)的人数有误，查询结果："+o.getIntValue("numCount")+",预期结果："+0;
                        msgArray.add(msg);
                    }
                    break;
                }

                case "ONLINE_TIME_21_TO_40S":{
                    if(o.getIntValue("numCount")!=num){
                        String msg="画图接口queryActivityTotalSta的在线时间(21-40S)的人数有误，查询结果："+o.getIntValue("numCount")+",预期结果："+num;
                        msgArray.add(msg);
                    }
                    break;
                }

                case "ONLINE_TIME_41_TO_60S":{
                    if(o.getIntValue("numCount")!=0){
                        String msg="画图接口queryActivityTotalSta的在线时间(41-60S)的人数有误，查询结果："+o.getIntValue("numCount")+",预期结果："+0;
                        msgArray.add(msg);
                    }
                    break;
                }

                case "ONLINE_TIME_61_TO_MAX":{
                    if(o.getIntValue("numCount")!=0){
                        String msg="画图接口queryActivityTotalSta的在线时间(61S-MAX)的人数有误，查询结果："+o.getIntValue("numCount")+",预期结果："+0;
                        msgArray.add(msg);
                    }
                    break;
                }
                default:break;

            }
        }
    }

    private void queryUserBehaviorDataCheck(JSONArray msgArray){
        JSONObject res;
        StatisticsNumType types[]={StatisticsNumType.OPEN_HOME_PAGE_COUNT,StatisticsNumType.OPEN_HOME_PAGE_USER_COUNT,
                StatisticsNumType.SHARE_FROM_WECHAT_TALKING,StatisticsNumType.SHARE_FROM_WECHAT_FRIEND_CIRCLE};
        for(StatisticsNumType type:types){
            UserBehaviorDataQueryDto param=new UserBehaviorDataQueryDto();
            param.setActId(Configs.getPageViewAlias);
            param.setDataCount(5);
            param.setTimeSpan(86400);
            param.setDataType(type);
            res=HttpUtil.put(Configs.portalBaseURL+Configs.queryUserBehaviorDataURL,null,param);
            if(res.getInteger("code")==200){
                if(res.getJSONArray("dataArray").size()>0){
                    //TODO 目前不知道数据返回的样子，这里还要做一下数据检查
                    switch (type){
                        case OPEN_HOME_PAGE_COUNT:{
                            break;
                        }
                        case OPEN_HOME_PAGE_USER_COUNT:{
                            break;
                        }
                        case SHARE_FROM_WECHAT_TALKING:{
                            break;
                        }
                        case SHARE_FROM_WECHAT_FRIEND_CIRCLE:{

                        }
                        default:break;
                    }
                }else{
                    String msg="数据接口queryUserBehaviorData返回空数组。"+"请求参数："+JSONObject.toJSONString(param);
                    msgArray.add(msg);
                }
            }
        }

    }

    private void shareDegreeDataCheck(JSONArray msgArray,JSONArray authkeys){
        //先生成分享广度和深度的数据。数据结果：10个人有传播，其中9个人深度和广度为1，1一个人深度为10，广度为1
        JSONObject res;
        PassDto passDto=new PassDto();
        passDto.setActivityId(Configs.getPageViewAlias);
        String fromOpenId;
        for(int i=0;i<10;i++){
            //这是从原链接打开
            fromOpenId="0";
            passDto.setFromOpenId(fromOpenId);
            passDto.setNickName("Original"+i);
            passDto.setOpenId(authkeys.getString(i));
            passDto.setProvince("广东");
            res= HttpUtil.post(Configs.engineBaseURL+Configs.passURL+fromOpenId,passDto,null);

            //这是从分享连接打开
            fromOpenId=authkeys.getString(i);
            passDto.setFromOpenId(fromOpenId);
            passDto.setNickName("T"+i);
            if(i>0){
                passDto.setOpenId("8358d4157c194291816de28943f522b"+i);
            }else{
                passDto.setOpenId("8358d4157c194291816de28943f522a0");
            }
            passDto.setProvince("广东");
            res= HttpUtil.post(Configs.engineBaseURL+Configs.passURL+fromOpenId,passDto,null);
        }

        //生产深度数据
        for(int i=0;i<10;i++){
            fromOpenId="8358d4157c194291816de28943f522a"+i;
            passDto.setFromOpenId(fromOpenId);
            passDto.setNickName("test"+i);
            passDto.setOpenId("8358d4157c194291816de28943f522a"+(i+1));
            passDto.setProvince("广东");
            res=HttpUtil.post(Configs.engineBaseURL+Configs.passURL+fromOpenId,passDto,null);

        }

        //检查数据
        res=HttpUtil.get(Configs.portalBaseURL+Configs.shareDegreeURL+Configs.getPageViewAlias,null);
        if(res.getInteger("code")==200){
            JSONArray breathArray=res.getJSONArray("breath");
            JSONArray deepArray=res.getJSONArray("deep");
            int maxDeep=res.getIntValue("maxDeep");

            if(maxDeep!=10){
                String msg="传播最大深度有误。查询结果："+maxDeep+",预期结果：10。";
                msgArray.add(msg);
            }
            if(breathArray.size()>0&&deepArray.size()>0){
                for(int i=0;i<10;i++){
                    int breathNum=breathArray.getJSONObject(i).getIntValue("total");
                    int deepNum=deepArray.getJSONObject(i).getIntValue("total");

                    int breathDegree=breathArray.getJSONObject(i).getIntValue("degree");
                    int deepDegree=deepArray.getJSONObject(i).getIntValue("degree");
                    //深度和广度为10的人数
                    if(breathDegree==10&&breathNum!=0){
                        String msg="广度为10的人数统计有误。查询结果："+breathNum+",预期结果：0";
                        msgArray.add(msg);
                    }
                    if(deepDegree==10&&deepNum!=1){
                        String msg="深度为10的人数统计有误。查询结果："+deepNum+",预期结果：1";
                        msgArray.add(msg);
                    }

                    //深度和广度为1的人数
                    if(breathDegree==1&&breathNum!=10){
                        String msg="广度为1的人数统计有误。查询结果："+breathNum+",预期结果：10";
                        msgArray.add(msg);
                    }
                    if(deepDegree==1&&deepNum!=9){
                        String msg="深度为1的人数统计有误。查询结果："+deepNum+"，预期结果：9";
                        msgArray.add(msg);
                    }

                    //其他度数的人数都是0
                    if(breathDegree!=1&&breathDegree!=10&&breathNum!=0){
                        String msg="广度为2-9的人数统计有误。查询结果："+breathNum+"预期结果：0";
                        msgArray.add(msg);
                    }
                    if(deepDegree!=1&&deepDegree!=10&&deepNum!=0){
                        String msg="深度为2-9的人数统计有误。查询结果："+deepNum+"预期结果：0";
                        msgArray.add(msg);
                    }
                }
            }else{
                String msg="shareDegree接口没有数据。";
                msgArray.add(msg);
            }
        }
        res=HttpUtil.get(Configs.portalBaseURL+Configs.shareDegreeTopTenRUL+Configs.getPageViewAlias,null);
        if(res.getInteger("code")==200){
            JSONArray resArray=res.getJSONArray("dataArray");
            if(resArray!=null&&resArray.size()>0){
                for(int i=0;i<resArray.size();i++){
                    JSONObject o=resArray.getJSONObject(i);
                    //产生的数据都是广度为1
                    if(o.getIntValue("widthLength")!=1){
                        String msg="传播前10的接口数据有误。广度查询结果为："+o.getIntValue("widthLength")+",预期结果：1";
                    }
                    if(o.getString("nickName").equals("Original0")){
                        if(o.getIntValue("treeLevel")!=10){
                            String msg="传播前10的接口数据有误。用户Original0的深度应为：10，查询结果却为："+o.getIntValue("treeLevel");
                            msgArray.add(msg);
                        }
                    }else{
                        if(o.getIntValue("treeLevel")!=1){
                            String msg="传播前10的接口数据有误。其他用户的深度应为：1，查询结果却为："+o.getIntValue("treeLevel");
                            msgArray.add(msg);
                        }
                    }
                }
            }else{
                String msg="传播前10的接口没数据返回。";
                msgArray.add(msg);
            }
        }
    }

    private void distributionDataCheck(JSONArray msgArray,int count){
        DataDimension dataDimensions[]={DataDimension.BROWSER,DataDimension.TERMINAL};
        for(DataDimension dataDimension:dataDimensions){
            JSONObject res=HttpUtil.get(Configs.portalBaseURL+Configs.distributionURL+Configs.getPageViewAlias+"/"+ dataDimension,null);
            if(res.getInteger("code")==200){
                JSONArray array=res.getJSONArray("dataArray");
                if(array!=null&&array.size()>0){
                    int value=array.getJSONObject(0).getIntValue("value");
                    if(value!=count){
                        String msg="distribution接口传入参数为"+dataDimension+"时，数据有误。查询结果："+value+",预期结果："+count;
                        msgArray.add(msg);
                    }

                }else{
                    String msg="distribution接口传入参数为"+dataDimension+"时，返回数据为空。";
                    msgArray.add(msg);
                }
            }
        }

        DataDimension dataDimensionArray[]={DataDimension.IP_PROVINCE,DataDimension.PHONE_PROVINCE};
        for(DataDimension dataDimension:dataDimensionArray){
            JSONObject res=HttpUtil.get(Configs.portalBaseURL+Configs.distributionURL+Configs.getPageViewAlias+"/"+ dataDimension,null);
            if(res.getInteger("code")==200){
                JSONArray array=res.getJSONArray("dataArray");
                if(array==null||array.size()==0){
                    String msg="distribution接口传入参数为"+dataDimension+"时，返回数据为空。";
                    msgArray.add(msg);
                }
            }
        }

    }

    public static void main(String[] args) {

//       JSONObject res=HttpUtil.get(Configs.portalBaseURL+Configs.shareDegreeTopTenRUL+Configs.getPageViewAlias,null);
//        System.out.println(res.getInteger("code"));
//        Random rand = new Random();
//        long l=rand.nextInt(1)+1;
//        System.out.println(l);

        DataStatisticsTestController c=new DataStatisticsTestController();
        JSONArray msg=new JSONArray();
        int i=199;
        c.distributionDataCheck(msg,i);
////        System.out.println(c.pageViewRequest().getInteger("code"));
//
//        PageViewDto pageViewDto=new PageViewDto();
//        pageViewDto.setOpenId("authkey");
//        pageViewDto.setActivityId("actId");
//        pageViewDto.setBrowserId("authkey");
//        pageViewDto.setStayTime(30L);
//        pageViewDto.setPlatformBrowserType(PlatformBrowserType.WECHAT);
//
//        List<PageActionType> typeList=new ArrayList<>();
//        typeList.add(PageActionType.CLICK_AD);
//        //分享的等英杰添加修改
////        typeList.add(PageActionType.SHARE);
//
//        pageViewDto.setActions(c.getActionDtoList(typeList,30));
//        String jsonObject=JSONObject.toJSONString(pageViewDto);
//        System.out.println(jsonObject);

//        String fromOpenId;
//        for(int i=0;i<10;i++){
//            fromOpenId="8358d4157c194291816de28943f522a"+i;
//            System.out.println(">>"+fromOpenId);
//            String openId="8358d4157c194291816de28943f522a"+(i+1);
//            System.out.println(">>>>"+openId);
//        }
//        String str="8358d4157c194291816de28943f522a10";
//        System.out.println(str.length());
    }
}
