package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.service.interfaces.*;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.customer.label.person.service.ICustomerQueryService;
import cn.com.dollar.customer.label.service.ILabelQueryService;
import cn.com.dollar.engine.act.group.service.IActGroupQueryService;
import cn.com.dollar.engine.act.parameter.model.type.ParameterKey;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Created by ZZJ on 2016-11-9.
 */
@RestController
public class testController {
    @Autowired
    TrophyItemResultService trophyItemResultService;
    @Autowired
    ActMainService actMainService;
    @Autowired
    ActParameterService actParameterService;
    @Autowired
    ShareCaseService shareCaseService;
    @Autowired
    ActivityTrophyMapperService activityTrophyMapperService;
    @Autowired
    ICustomerQueryService customerQueryService;
    @Autowired
    ILabelQueryService labelQueryService;
    @Autowired
    IActGroupQueryService actGroupQueryService;

    private ResponseUtil responseUtil=new ResponseUtil();

    @RequestMapping(value = "/test")
    public String test(){
//        Map<String ,String> map=new HashMap<String, String>();
//        map.put("1","hello world!");
//        List<TrophyItemInfo> list=trophyItemResultService.querySharedRecords(10065l);
        return "ok";
    }

    @RequestMapping(value = "/getDayDrawTimesCheck/{actId}",method = RequestMethod.GET)
    public Map getDayDrawTimesCheck(@PathVariable BigDecimal actId,@RequestParam(required = false) boolean shareAndOpen){
        int dayDrawTimes=actParameterService.getActParameter(actId, ParameterKey.PART_DAY_TIMES);
        dayDrawTimes=trophyItemResultService.getGameTimes(actId,dayDrawTimes,shareAndOpen,"ADD_JOIN_TIMES");
        List<Integer> result=trophyItemResultService.drawTimesCheck(actId,dayDrawTimes);
        return responseUtil.sendResult(result);
    }

    @RequestMapping(value = "/getDayLotteryTimesCheck/{actId}",method = RequestMethod.GET)
    public Map getDayLotteryTimesCheck(@PathVariable BigDecimal actId,@RequestParam(required = false) boolean shareAndOpen){
        int dayLotteryTimes=actParameterService.getActParameter(actId, ParameterKey.LOTTERY_DAY_TIMES);
        dayLotteryTimes=trophyItemResultService.getGameTimes(actId,dayLotteryTimes,shareAndOpen,"ADD_EXTRA_MONEY");
        List<Integer> result=trophyItemResultService.lotteryTimesCheck(actId,dayLotteryTimes);
        return responseUtil.sendResult(result);
    }

    @RequestMapping(value = "/getCallerIpCheck/{actId}",method = RequestMethod.GET)
    public Map getCallerIpCheck(@PathVariable BigDecimal actId){
        List<String> result=trophyItemResultService.callerIpCheck(actId);
        return responseUtil.sendResult(result);
    }

    @RequestMapping(value = "/getCredentialWithPhones/{actId}",method = RequestMethod.GET)
    public Map getCredentialWithPhones(@PathVariable BigDecimal actId){
        List<Integer> result=trophyItemResultService.credentialWithPhones(actId);
        return responseUtil.sendResult(result);
    }

    @RequestMapping(value = "/getPhoneWithCredential/{actId}",method = RequestMethod.GET)
    public Map getPhoneWithCredential(@PathVariable BigDecimal actId){
        List<String> result=trophyItemResultService.phoneWithCredential(actId);
        return responseUtil.sendResult(result);
    }

    @RequestMapping(value = "/getOverTimeItems/{actId}",method = RequestMethod.GET)
    public Map getOverTimeItems(@PathVariable BigDecimal actId) throws ParseException {
        Date date=actMainService.getActEndDate(actId);
        List<Integer> result=trophyItemResultService.overTimeItems(actId,date);
        return responseUtil.sendResult(result);
    }

    @RequestMapping(value = "/getIllegalPhoneCheck/{actId}",method = RequestMethod.GET)
    public Map getIllegalPhoneCheck(@PathVariable BigDecimal actId){
        List<String> illegalPhones=trophyItemResultService.getIllegalPhone(actId);
        return responseUtil.sendResult(illegalPhones);
    }

    @RequestMapping(value = "/getTrophyLeftCheck/{actId}",method = RequestMethod.GET)
    public Map getTrophyLeftCheck(@PathVariable BigDecimal actId){
        return activityTrophyMapperService.getTrophyLeftCheck(actId);

    }





    public static void main(String [] args){
        DrawDto drawDto=new DrawDto();
        drawDto.setActivityId(10055L);
        drawDto.setAuthKey("ed5aa14a32c44533af5f101869fd3e13");
        drawDto.setType(CredentialType.WECHAT);
        drawDto.setPhone("13796894658");
        JSONObject json=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+json.toString());

        JSONObject getJson=HttpUtil.get(Configs.engineBaseURL+Configs.drawURL+"?activityId=10055&type=WECHAT&authKey=ed5aa14a32c44533af5f101869fd3e13",null);
        System.out.println(">>>>>>>>>"+getJson.toString());
    }
}


