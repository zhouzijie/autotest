package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.model.ProjectProperties;
import cn.com.dollar.autoTest.service.implement.ActMainServiceImp;
import cn.com.dollar.autoTest.service.implement.ConfigurationService;
import cn.com.dollar.autoTest.service.interfaces.ActParameterService;
import cn.com.dollar.autoTest.service.interfaces.TrophyItemResultService;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.engine.act.main.service.IActOperateService;
import cn.com.dollar.engine.trophy.item.model.TrophyItem;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ZZJ on 2017-2-17.
 */
@RestController
public class UserVertificationAndFilterationTestController {
    @Autowired
    ActParameterService actParameterService;
    @Autowired
    TrophyItemResultService trophyItemResultService;
    @Autowired
    IActOperateService actOperateService;
    private ResponseUtil responseUtil=new ResponseUtil();

    //ip地址过滤逻辑：超过ip限制次数，就不会中奖，返回谢谢参与，而且会产生数据库记录
    @RequestMapping(value = "/ipTimesFilteratoinTest")
    public Map ipTimesFilteratoinTest() throws UnsupportedEncodingException {
        ProjectProperties engineProperties= ConfigurationService.getEngineConfiguration();
        BigDecimal ipLimit=engineProperties.getIpWarningReqTimes();
        JSONObject res;
        //设置抽奖参数
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.ipTestActId);
        drawDto.setAuthKey("13928248280");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248280");
        //传了这个才能抽到分组的奖品
        drawDto.setAppId(Configs.companyId);

        //设置活动的可中奖和可抽奖次数为(ipLimit+5)
        actParameterService.updateActParameter(drawDto.getActivityId(),ipLimit,"5");
        //循环抽奖,次数超过IP限制次数。PS：活动的中奖率应该设为100%
        for(int i=0;i<ipLimit.intValue()+5;i++){
            //抽奖结果
            res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        }

        //抽完后检查结果，如果ip过滤有效，中奖记录条数等于ipLimit。超过的都为谢谢参与。
        int times=trophyItemResultService.getLotteryTimesByCredentialValue(drawDto.getActivityId(),drawDto.getAuthKey());
        if(times>ipLimit.intValue()){
            return responseUtil.sendResult(false,"ip限制失效，ip限制次数为："+ipLimit+",中奖次数为："+times);
        }

        //先清除数据，这里如果调用service的话，就不能在本地跑，因为service里的命令是linux的。
        // actOperateService.cleanTestData(Configs.ipTestActId);
        //想本地跑，只能用接口的形式。
        res= ActMainServiceImp.getLoginMessage();
        res=HttpUtil.delete(Configs.portalBaseURL+Configs.cleanTestDataURL+"/"+Configs.ipTestActId,res.getString("access_token"));
        if(res.getInteger("code")==200){
            //等1秒，以免数据还没清除完
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //下面是在高并发的情况下测试ip限制
            ExecutorService pool = Executors.newCachedThreadPool();
            int concurrent=1000;
            for(int i=0;i<concurrent;i++){
                pool.execute(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
                    }
                });
            }
            pool.shutdown();
            System.out.println("shutdown()：启动一次顺序关闭，执行以前提交的任务，但不接受新任务。");
            while(true){
                if(pool.isTerminated()){
                    System.out.println("所有的子线程都结束了！");
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //抽完后检查结果，如果ip过滤有效，中奖记录条数等于ipLimit。超过的都为谢谢参与。
            times=trophyItemResultService.getLotteryTimesByCredentialValue(drawDto.getActivityId(),drawDto.getAuthKey());
            if(times>ipLimit.intValue()){
                return responseUtil.sendResult(false,"在"+concurrent+"并发下，ip限制失效，ip限制次数为："+ipLimit+",中奖次数为："+times);
            }
        }else{
            responseUtil.sendResult(false,"并发测试执行失败，清除数据出错："+res.getJSONObject("msg"));
        }
        return responseUtil.sendResult(true,"");
    }

    @RequestMapping(value = "/phoneFilterationTest")
    //该测试需要把信息采集的手机号一栏打开，否则不会检验手机号的合法性
    public Map phoneFilterationTest(){
        DrawDto drawDto=new DrawDto();

        //这次抽奖是phone字段的值有问题
        drawDto.setActivityId(Configs.userVertifyActId);
        drawDto.setAuthKey("13928248281");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("12345678901");
        JSONObject res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getInteger("code")==200){
            System.out.println(res.getString("msg"));

            List<TrophyItem> list= trophyItemResultService.getTrophyItemByPhone(drawDto.getActivityId(),drawDto.getPhone());
            if(list.size()>0){
                return responseUtil.sendResult(false,"手机号验证失效，号码为："+drawDto.getPhone());
            }
        }else if(res.getInteger("code")>=500){
            return responseUtil.sendResult(false,"抽奖接口出错："+res.getString("msg"));
        }
        //这次抽奖是凭证值非法
        drawDto.setAuthKey("12345678901");
        drawDto.setPhone("13928248281");
        res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getInteger("code")==200){
            System.out.println(res.getString("msg"));

            List<TrophyItem> lists=trophyItemResultService.getTrophyItemByCredential(drawDto.getActivityId(),drawDto.getAuthKey());
            if(lists.size()>0){
                return responseUtil.sendResult(false,"手机号验证失效，凭证（authkey）值为："+drawDto.getAuthKey());
            }
        }else if(res.getInteger("code")>=500){
            return responseUtil.sendResult(false,"抽奖接口出错："+res.getString("msg"));
        }
        return responseUtil.sendResult(true,"");
    }

    @RequestMapping(value = "/openIdFilterationTest")
    public Map openIdFilterationTest(){
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.userVertifyActId);
        drawDto.setAuthKey("13928248290");
        drawDto.setType(CredentialType.WECHAT);
        drawDto.setPhone("13928248290");
        JSONObject res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);

        if(res.getInteger("code")==200){
            return responseUtil.sendResult(false,"openID检测失效，传入的值为："+drawDto.getAuthKey());
        }else{
            return responseUtil.sendResult(true,"");
        }
    }

    @RequestMapping(value = "/phoneAndOpenIdBindTest")
    public Map phoneAndOpenIdBindTest(){
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.userVertifyActId);
        drawDto.setAuthKey("13928248291");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248291");

        JSONObject res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getIntValue("code")==200){
            drawDto.setAuthKey("13928248288");
            res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
            if(res.getIntValue("code")==200){
                return responseUtil.sendResult(false,"手机号和凭证值绑定失效，传入的值为：authkey-"+drawDto.getAuthKey()+"phone-"+drawDto.getPhone());
            }

            drawDto.setAuthKey("13928248291");
            drawDto.setPhone("13928248288");
            res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
            if(res.getIntValue("code")==200){
                return responseUtil.sendResult(false,"手机号和凭证值绑定失效，传入的值为：authkey-"+drawDto.getAuthKey()+"phone-"+drawDto.getPhone());
            }
        }
        return responseUtil.sendResult(true,"");
    }

    @RequestMapping(value = "/signCheckTest")
    public Map signCheckTest(){
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.userVertifyActId);
        drawDto.setAuthKey("13928248692");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248692");

        String sign="123456";

        JSONObject res=HttpUtil.post(Configs.engineBaseURL+Configs.drawURL+"?sign="+sign,drawDto,null);
        if(res.getIntValue("code")==200){
            return responseUtil.sendResult(false,"sign检验失效，sign值为："+sign);
        }
        return responseUtil.sendResult(true,"");

    }
}
