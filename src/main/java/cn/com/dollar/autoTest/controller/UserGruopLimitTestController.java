package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.customer.label.person.model.CustomerInfo;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.customer.label.person.service.ICustomerQueryService;
import cn.com.dollar.customer.label.service.ILabelQueryService;
import cn.com.dollar.engine.act.group.service.IActGroupQueryService;
import cn.com.dollar.engine.trophy.main.model.GroupRuleInfo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * Created by ZZJ on 2017-2-24.
 */
@RestController
public class UserGruopLimitTestController {
    private ResponseUtil responseUtil=new ResponseUtil();

    @Autowired
    ILabelQueryService labelQueryService;
    @Autowired
    IActGroupQueryService actGroupQueryService;
    @Autowired
    ICustomerQueryService customerQueryService;
    //用户分组限制测试
    @RequestMapping(value = "/getUserGroupCheck")
    public Map getUserGroupCheck(){
        //设置参数
        DrawDto drawDto=new DrawDto();
        drawDto.setActivityId(Configs.userGroupActId);
        drawDto.setAuthKey("13710947804");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13710947804");
        //appId就是该活动所属的企业ID
        drawDto.setAppId(Configs.companyId);
        JSONObject postJson= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);

        JSONArray array=postJson.getJSONArray("trophyItems");
        if(!array.getJSONObject(0).getString("trophyType").equals("THANK_YOU")){
            //查询奖品的分组，获取组别iD
            List<GroupRuleInfo> groupRuleInfos=actGroupQueryService.getGroupConditions(drawDto.getActivityId(),array.getJSONObject(0).getLong("trophyId"));
            //获取用户信息
            CustomerInfo customerInfo=customerQueryService.getInfoByCredential(drawDto.getAppId(),drawDto.getType(),drawDto.getAuthKey());
            for(GroupRuleInfo info : groupRuleInfos){
                Long groupId=info.getGroupId();

                //获取分组的标签
                List<Long> labelIds=actGroupQueryService.getGroupLabelIds(groupId);
                //判断用户是否属于奖品所在的分组
                for(int i=0;i<labelIds.size();i++){
                    if(labelQueryService.isContains(drawDto.getAppId(),labelIds.get(i),customerInfo.customerId)){
                        return responseUtil.sendResult(true,"");
                    }
                }

            }



            return responseUtil.sendResult(false,"该奖品(trophyId):"+array.getJSONObject(0).getInteger("trophyId")+",不属于该用户(AuthKey):"+drawDto.getAuthKey());
        }else{
            return responseUtil.sendResult(false,"用分组里的用户抽奖，且中奖率设置为100，但是并没有中奖。");
        }

    }
}
