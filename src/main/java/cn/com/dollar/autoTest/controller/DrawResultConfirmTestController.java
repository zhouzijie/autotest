package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by ZZJ on 2017-2-24.
 */
@RestController
public class DrawResultConfirmTestController {
    private ResponseUtil responseUtil=new ResponseUtil();

    //抽奖结果对比测试
    @RequestMapping(value = "/getDrawResultCheck",method = RequestMethod.GET)
    public Map getDrawResultCheck(){
        //设置参数
        DrawDto drawDto=new DrawDto();
        drawDto.setActivityId(Configs.userVertifyActId);
        drawDto.setAuthKey("13928248297");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248297");

        //抽奖结果
        JSONObject postJson= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        //查奖结果
        JSONObject getJson=HttpUtil.get(Configs.engineBaseURL+Configs.drawURL+"?activityId="+drawDto.getActivityId()+"&type="+drawDto.getType()+
                "&authKey="+drawDto.getAuthKey(),null);

        if(postJson.getInteger("code")!=200){
            return responseUtil.sendResult(false,postJson.get("msg").toString());
        }
        if(getJson.getInteger("code")!=200){
            return responseUtil.sendResult(false,getJson.get("msg").toString());
        }

        JSONArray postArray=postJson.getJSONArray("trophyItems");
        JSONArray getArray=getJson.getJSONArray("trophyItems");


        JSONObject p=postArray.getJSONObject(0);
        Integer trophyId=p.getInteger("trophyId");

        for(int i=0;i<getArray.size();i++){
            if(getArray.getJSONObject(i).getInteger("trophyId").equals(trophyId)){
                String getStr=getArray.getJSONObject(i).toString();
                String postStr=p.toString();
                if(getStr.equals(postStr)){
                    return responseUtil.sendResult(true,"");
                }else{
                    return responseUtil.sendResult(false,"抽奖结果："+postStr+",查奖结果"+getStr);
                }
            }
        }

        return responseUtil.sendResult(false,"找不到对应的抽奖结果");

    }
}
