package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.service.implement.ActMainServiceImp;
import cn.com.dollar.autoTest.service.interfaces.ActMainService;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.engine.act.main.model.ActivityMainInfo;
import cn.com.dollar.engine.act.main.model.type.ActStatus;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

/**
 * Created by ZZJ on 2017-4-25.
 */
@RestController
@RequestMapping(value = "/clean")
public class DeleteDataController {
    private ResponseUtil responseUtil=new ResponseUtil();
    @Autowired
    ActMainService actMainService;

    @RequestMapping(value = "/all")
    public Map cleanAllData() throws UnsupportedEncodingException {
        JSONObject res= ActMainServiceImp.getLoginMessage();
        String token=res.getString("access_token");

//        for(Long actId:Configs.actIds){
//            //该活动要先更新actmain才能清除数据
//            if(actId.equals(Configs.actTimeLimitActId)){
//                ActivityMainInfo activityMainInfo=new ActivityMainInfo();
//                activityMainInfo.actStatus= ActStatus.UN_PUBLISH;
//                activityMainInfo.startDate=new Date(127,9,9);
//                activityMainInfo.endDate=new Date(230,8,8);
//                activityMainInfo.actId=Configs.actTimeLimitActId;
//                actMainService.updateAct(activityMainInfo);
//            }
//            res= HttpUtil.delete(Configs.portalBaseURL+Configs.cleanTestDataURL+"/"+actId,token);
//            if(res.getInteger("code")!=200){
//                return responseUtil.sendResult(false,"清除失败。");
//            }
//        }
//
//        //清除创建活动模块的数据
//        if(!actMainService.deleteCreateActivityTestData()){
//            return responseUtil.sendResult(false,"清除创建活动模块的数据失败。");
//        }

        if(!actMainService.deletePageViewTestData(token)){
            return responseUtil.sendResult(false,"清除统计数据活动的数据失败。");
        }
        return responseUtil.sendResult(true,"");
    }

}
