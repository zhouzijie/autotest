package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.*;
import cn.com.dollar.autoTest.service.implement.ActMainServiceImp;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.engine.act.group.model.ActGroupInfo;
import cn.com.dollar.engine.act.group.service.ActGroupDto;
import cn.com.dollar.engine.act.main.dto.CheckInfoJson;
import cn.com.dollar.engine.act.main.dto.InfoCollect;
import cn.com.dollar.engine.act.main.model.type.ActPin;
import cn.com.dollar.engine.act.main.model.type.DeliverSwitch;
import cn.com.dollar.engine.act.share.type.ShareBuffType;
import cn.com.dollar.engine.act.share.type.WorkCondition;
import cn.com.dollar.engine.trophy.main.model.TrophyInfo;
import cn.com.dollar.engine.trophy.main.model.type.LimitDepend;
import cn.com.dollar.engine.trophy.main.model.type.TrophyType;
import cn.com.dollar.engine.trophy.main.service.ITrophyQueryService;
import cn.com.dollar.engine.trophy.main.service.TrophyDto;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by ZZJ on 2017-4-6.
 */
@RestController
public class CreateActivityTestController {
    @Autowired
    ITrophyQueryService trophyQueryService;

    private ResponseUtil responseUtil=new ResponseUtil();
    @RequestMapping(value = "/createActivity")
    public Map createActivity() throws UnsupportedEncodingException {
        //创建活动需要先登录，拿到token
        JSONObject res=ActMainServiceImp.getLoginMessage();

        JSONArray msgArray=new JSONArray();
        if(res.getInteger("code")==200){
            //创建活动
            String token=res.getString("access_token");
            ActivityMainDto dto=new ActivityMainDto();
            dto.setActName("AT_创建活动测试");
            dto.setActDesc("创建测试");
            dto.setCompanyId("34eb35f02786481580e4541db1e9054e");
            dto.setDeliverSwitch(DeliverSwitch.OFF);
            dto.setStartDate(new Date());
            dto.setEndDate(new Date(300,2,2));
            res=HttpUtil.post(Configs.portalBaseURL+Configs.createMainURL,dto,token);

            if(res.getInteger("code")==201){
                res.remove("code");
                ActivityMainDto activityMainDto=JSONObject.parseObject(res.toJSONString(),ActivityMainDto.class);

                Long actId=res.getLong("actId");
                String alias=res.getString("alias");

                //修改活动参数
                ActParameterDto actParameterDto=new ActParameterDto();
                actParameterDto.setActId(actId);
                actParameterDto.setLotteryDayTimes(new BigDecimal("5"));
                actParameterDto.setLotteryTotalTimes(new BigDecimal("5"));
                actParameterDto.setPartDayTimes(new BigDecimal("3"));
                actParameterDto.setPartTotalTimes(new BigDecimal("3"));
                res=HttpUtil.put(Configs.portalBaseURL+Configs.actParameterURL+"/"+actId,token,actParameterDto);
                if(res.getInteger("code")!=200){
                    String msg="修改活动参数出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }

                //修改活动信息
                activityMainDto.setActName("修改活动名字");
                activityMainDto.setActDesc("修改活动描述");
                activityMainDto.setStartDate(new Date(117,9,9));
                activityMainDto.setEndDate(new Date(230,8,8));
                activityMainDto.setDeliverSwitch(DeliverSwitch.ON);
                activityMainDto.setActPin(ActPin.STAR);

                CheckInfoJson checkInfoJson=new CheckInfoJson();
                checkInfoJson.setEnableInfoCollect(true);
                checkInfoJson.setRequirePicCode(true);
                checkInfoJson.setRequireWechatInfo(true);

                InfoCollect infoCollect=new InfoCollect();
                infoCollect.setRequireIdNo(true);
                infoCollect.setRequireName(true);
                infoCollect.setRequirePhone(true);
                infoCollect.setRequireSms(true);
                checkInfoJson.setInfoCollect(infoCollect);

                activityMainDto.setCheckInfoJson(checkInfoJson);

                res=HttpUtil.put(Configs.portalBaseURL+Configs.createMainURL+"/"+actId,token,activityMainDto);
                if(res.getInteger("code")!=200){
                    String msg="修改活动信息出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }

                //添加奖品
                TrophyDto trophyDto=new TrophyDto();
                trophyDto.setActId(actId);
                trophyDto.setNote("添加奖品");
                trophyDto.setTrophyLimit(new BigDecimal(100));
                trophyDto.setTrophyName("奖品B");
                trophyDto.setTrophyType(TrophyType.CURRENT_SHARE);
                trophyDto.setTrophyWorth(new BigDecimal(10));
                trophyDto.setTrophyDetail(JSONObject.toJSONString(TrophyDetail.getDefault()));

                res=HttpUtil.post(Configs.portalBaseURL+Configs.trophyURL,trophyDto,null);
                if(res.getInteger("code")!=201){
                    String msg="添加奖品出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }
                //添加用户组
                ActGroupInfo actGroupInfo=new ActGroupInfo();
                actGroupInfo.actId=actId;
                actGroupInfo.groupName="用户分组测试";
                res=HttpUtil.post(Configs.portalBaseURL+Configs.groupURL,actGroupInfo,token);
                if(res.getInteger("code")!=201){
                    String msg="添加用户分组出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }else{
                    //给用户组添加奖品
                    GroupRuleDto groupRuleDto=new GroupRuleDto();
                    groupRuleDto.setGroupId(res.getLong("groupId"));
                    groupRuleDto.setActId(actId);
                    groupRuleDto.setLimitDepend(LimitDepend.GROUP);
                    groupRuleDto.setQuantityLimit(new BigDecimal(20));
                    groupRuleDto.setWeight(new BigDecimal(100));
                    List<TrophyInfo> list= trophyQueryService.queryTrophyList(actId,1,100).content;
                    groupRuleDto.setTrophyId(list.get(0).trophyId);
                    res=HttpUtil.post(Configs.portalBaseURL+Configs.groupRuleURL,groupRuleDto,token);
                    if(res.getInteger("code")!=201){
                        String msg="给用户组添加奖品出错："+res.getJSONObject("msg");
                        msgArray.add(msg);
                    }

                    //修改用户组的奖品信息
                    groupRuleDto.setLimitDepend(LimitDepend.TROPHY);
                    groupRuleDto.setWeight(new BigDecimal(50));
                    res=HttpUtil.put(Configs.portalBaseURL+Configs.groupRuleURL,token,groupRuleDto);
                    if(res.getInteger("code")!=200){
                        String msg="修改用户组的奖品信息出错："+res.getJSONObject("msg");
                        msgArray.add(msg);
                    }

                    //添加用户组的标签
                    LabelDto labelDto=new LabelDto();
                    labelDto.setCreator("AutoTest");
                    labelDto.setLabelDesc("");
                    labelDto.setLabelName("测试标签A");
                    res=HttpUtil.post(Configs.portalBaseURL+Configs.labelURL,labelDto,token);
                    if(res.getInteger("code")!=201){
                        String msg="添加用户组的标签出错："+res.getJSONObject("msg");
                        msgArray.add(msg);
                    }
                    //上传标签用户
                    Long labelId=res.getLong("labelId");
                    File file=new File(Configs.userFileAddress);
                    Map<String,Object> param=new HashMap<>();
                    param.put("labelIds",labelId);
                    res=HttpUtil.postFile(Configs.portalBaseURL+Configs.uploadLabelUsersURL,param,file,"fileUpload",token);
                    if(res.getInteger("code")!=200){
                        String msg="上传标签用户出错："+res.getJSONObject("msg");
                        msgArray.add(msg);
                    }

                    //导入标签用户
                    List<ActGroupDto> actGroupDtoList=new ArrayList<ActGroupDto>();

                    ActGroupDto actGroupDto = new ActGroupDto();
                    actGroupDto.setGroupId(groupRuleDto.getGroupId());
                    actGroupDto.setActId(actId);
                    actGroupDto.setGroupName(actGroupInfo.groupName);
                    List<Long> labelIdArray=new ArrayList<>();
                    labelIdArray.add(labelId);
                    actGroupDto.setLabelIdArray(labelIdArray);

                    actGroupDtoList.add(actGroupDto);
                    res=HttpUtil.post(Configs.portalBaseURL+Configs.importLabelsURL,actGroupDtoList,token);
                    if(res.getInteger("code")!=201){
                        String msg="导入标签到用户组出错："+res.getJSONObject("msg");
                        msgArray.add(msg);
                    }
                }

                //添加分享方案
                PortalShareCaseDto portalShareCaseDto=new PortalShareCaseDto();
                portalShareCaseDto.setActId(actId);
                portalShareCaseDto.setShareBuffTimes(1);
                portalShareCaseDto.setShareBuffType(ShareBuffType.ADD_JOIN_TIMES);
                portalShareCaseDto.setShareBuffValue(new BigDecimal(1));
                portalShareCaseDto.setWorkCondition(WorkCondition.ONLY_SHARE);
                portalShareCaseDto.setWorkTimes(1);
                res=HttpUtil.post(Configs.portalBaseURL+Configs.shareCaseURL,portalShareCaseDto,token);
                if(res.getInteger("code")!=200){
                    String msg="添加分享方案出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }
                //修改分享方案
                Integer shareCaseId=res.getInteger("shareCaseId");
                portalShareCaseDto.setShareBuffTimes(2);
                portalShareCaseDto.setShareBuffValue(new BigDecimal(2));
                portalShareCaseDto.setWorkTimes(2);
                portalShareCaseDto.setWorkCondition(WorkCondition.SHARE_AND_OPEN);
                res=HttpUtil.put(Configs.portalBaseURL+Configs.shareCaseURL+"/"+shareCaseId,token,portalShareCaseDto);
                if(res.getInteger("code")!=200){
                    String msg="修改分享方案出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }
                //添加渠道
                ChannelDto channelDto=new ChannelDto();
                channelDto.setActId(actId);
                channelDto.setChannelName("WX");
                res=HttpUtil.post(Configs.portalBaseURL+Configs.channelURL,channelDto,token);
                if(res.getInteger("code")!=201){
                    String msg="添加渠道出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }
                //修改渠道信息
                channelDto.setChannelId(res.getLong("channelId"));
                channelDto.setChannelName("TEST_CHANNEL");
                res=HttpUtil.put(Configs.portalBaseURL+Configs.channelURL,token,channelDto);
                if(res.getInteger("code")!=201){
                    String msg="修改渠道信息出错："+res.getJSONObject("msg");
                    msgArray.add(msg);
                }
            }else{
                return  responseUtil.sendResult(false,"创建活动失败"+res.getJSONObject("msg"));
            }
        }else{
            return responseUtil.sendResult(false,"登录失败。"+res.getJSONObject("msg"));
        }
        
        if(msgArray.size()!=0){
            return responseUtil.sendResult(false,msgArray);
        }else{
            return responseUtil.sendResult(true,"");
        }

    }

    public static void main(String[] args) {
//        Date d=new Date(300,2,2);
//        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
//
//        System.out.println(format.format(d));

//        TrophyDto trophyDto=new TrophyDto();
//        trophyDto.setActId(13L);
//        trophyDto.setNote("添加奖品");
//        trophyDto.setTrophyLimit(new BigDecimal(100));
//        trophyDto.setTrophyName("奖品B");
//        trophyDto.setTrophyType(TrophyType.CURRENT_SHARE);
//        trophyDto.setTrophyWorth(new BigDecimal(10));
//        trophyDto.setTrophyDetail(JSONObject.toJSONString(TrophyDetail.getDefault()));
//
//        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.trophyURL,trophyDto,null);
//        if(res.getInteger("code")!=201){
//            String msg="添加奖品出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }


//        Long labelId=res.getLong("labelId");
//        File file=new File("C:\\Users\\ZZJ\\Desktop\\用户分组模板.xlsx");
//        Map<String,Object> param=new HashMap<>();
//        param.put("labelIds",23L);
//        JSONObject   res=HttpUtil.postFile(Configs.portalBaseURL+Configs.uploadLabelUsersURL,param,file,"fileUpload","3b6bfcfc8bb96003824b92b078a3c7b2");
//        if(res.getInteger("code")!=200){
//            String msg="上传标签用户出错："+res.getJSONObject("msg");
////            msgArray.add(msg);
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

        List<ActGroupDto> actGroupDtoList=new ArrayList<ActGroupDto>();

        ActGroupDto actGroupDto = new ActGroupDto();
        actGroupDto.setGroupId(194L);
        actGroupDto.setActId(35L);
        actGroupDto.setGroupName("hahahahahahah");
        List<Long> labelIdArray=new ArrayList<>();
        labelIdArray.add(23L);
        actGroupDto.setLabelIdArray(labelIdArray);

        actGroupDtoList.add(actGroupDto);
        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.importLabelsURL,actGroupDtoList,"3b6bfcfc8bb96003824b92b078a3c7b2");
        if(res.getInteger("code")!=201){
            String msg="导入标签到用户组出错："+res.getJSONObject("msg");
            System.out.println(msg);
        }else{
            System.out.println(res.toString());
        }

//        Long actId=34L;
//        ActParameterDto actParameterDto=new ActParameterDto();
//        actParameterDto.setActId(actId);
//        actParameterDto.setLotteryDayTimes(new BigDecimal("5"));
//        actParameterDto.setLotteryTotalTimes(new BigDecimal("5"));
//        actParameterDto.setPartDayTimes(new BigDecimal("5"));
//        actParameterDto.setPartTotalTimes(new BigDecimal("5"));
//        JSONObject res=HttpUtil.put(Configs.portalBaseURL+Configs.actParameterURL+"/"+actId,"3b6bfcfc8bb96003824b92b078a3c7b2",actParameterDto);
//        if(res.getInteger("code")!=200){
//            String msg="修改活动参数出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        ActGroupInfo actGroupInfo=new ActGroupInfo();
//        actGroupInfo.actId=34L;
//        actGroupInfo.groupName="用户分组测试";
//        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.groupURL,actGroupInfo,"3b6bfcfc8bb96003824b92b078a3c7b2");
//        if(res.getInteger("code")!=201){
//            String msg="添加用户分组出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        GroupRuleDto groupRuleDto=new GroupRuleDto();
//        groupRuleDto.setGroupId(193L);
//        groupRuleDto.setActId(34L);
//        groupRuleDto.setLimitDepend(LimitDepend.GROUP);
//        groupRuleDto.setQuantityLimit(new BigDecimal(20));
//        groupRuleDto.setWeight(new BigDecimal(100));
////        List<TrophyInfo> list= trophyQueryService.queryTrophyList(actId,1,100).content;
//        groupRuleDto.setTrophyId(41L);
//        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.groupRuleURL,groupRuleDto,"3b6bfcfc8bb96003824b92b078a3c7b2");
//        if(res.getInteger("code")!=201){
//            String msg="给用户组添加奖品出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        groupRuleDto.setLimitDepend(LimitDepend.TROPHY);
//        groupRuleDto.setWeight(new BigDecimal(50));
//        JSONObject res=HttpUtil.put(Configs.portalBaseURL+Configs.groupRuleURL,"3b6bfcfc8bb96003824b92b078a3c7b2",groupRuleDto);
//        if(res.getInteger("code")!=200){
//            String msg="修改用户组的奖品信息出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        LabelDto labelDto=new LabelDto();
//        labelDto.setCreator("ceshi");
//        labelDto.setLabelDesc("");
//        labelDto.setLabelName("测试标签ABC");
//        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.labelURL,labelDto,"3b6bfcfc8bb96003824b92b078a3c7b2");
//        if(res.getInteger("code")!=201){
//            String msg="添加用户组的标签出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        PortalShareCaseDto portalShareCaseDto=new PortalShareCaseDto();
//        portalShareCaseDto.setActId(34L);
//        portalShareCaseDto.setShareBuffTimes(1);
//        portalShareCaseDto.setShareBuffType(ShareBuffType.ADD_JOIN_TIMES);
//        portalShareCaseDto.setShareBuffValue(new BigDecimal(1));
//        portalShareCaseDto.setWorkCondition(WorkCondition.ONLY_SHARE);
//        portalShareCaseDto.setWorkTimes(1);
//        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.shareCaseURL,portalShareCaseDto,"3b6bfcfc8bb96003824b92b078a3c7b2");
//        if(res.getInteger("code")!=200){
//            String msg="添加分享方案出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        Integer shareCaseId=74;
//        portalShareCaseDto.setShareBuffTimes(2);
//        portalShareCaseDto.setShareBuffValue(new BigDecimal(2));
//        portalShareCaseDto.setWorkTimes(2);
//        portalShareCaseDto.setWorkCondition(WorkCondition.SHARE_AND_OPEN);
//        JSONObject res=HttpUtil.put(Configs.portalBaseURL+Configs.shareCaseURL+"/"+shareCaseId,"3b6bfcfc8bb96003824b92b078a3c7b2",portalShareCaseDto);
//        if(res.getInteger("code")!=200){
//            String msg="修改分享方案出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        ChannelDto channelDto=new ChannelDto();
//        channelDto.setActId(34L);
//        channelDto.setChannelName("WX");
//        JSONObject res=HttpUtil.post(Configs.portalBaseURL+Configs.channelURL,channelDto,"3b6bfcfc8bb96003824b92b078a3c7b2");
//        if(res.getInteger("code")!=201){
//            String msg="添加渠道出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }

//        channelDto.setChannelId(38L);
//        channelDto.setChannelName("TEST_CHANNEL");
//        JSONObject res=HttpUtil.put(Configs.portalBaseURL+Configs.channelURL,"3b6bfcfc8bb96003824b92b078a3c7b2",channelDto);
//        if(res.getInteger("code")!=201){
//            String msg="修改渠道信息出错："+res.getJSONObject("msg");
//            System.out.println(msg);
//        }else{
//            System.out.println(res.toString());
//        }
    }
}
