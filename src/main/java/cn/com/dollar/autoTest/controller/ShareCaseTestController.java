package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.Dto.ShareCaseDto;
import cn.com.dollar.autoTest.service.interfaces.TrophyItemResultService;
import cn.com.dollar.autoTest.util.ActRedisKeyManage;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.engine.act.share.model.ShareCaseInfo;
import cn.com.dollar.engine.act.share.service.IShareCaseQueryService;
import cn.com.dollar.engine.act.share.type.ShareBuffType;
import cn.com.dollar.engine.act.share.type.WorkCondition;
import cn.com.dollar.engine.trophy.item.model.TrophyItemInfo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ZZJ on 2017-3-7.
 */
@RestController
//该测试需要建立一个有分享方案的活动。这个测试一个活动应该测不完，需要至少2个活动
public class ShareCaseTestController {
    @Autowired
    IShareCaseQueryService shareCaseQueryService;
    @Autowired
    ActRedisKeyManage actRedisKeyManage;
    @Autowired
    TrophyItemResultService trophyItemResultService;
    private ResponseUtil responseUtil=new ResponseUtil();
    @RequestMapping(value = "/shareCaseEffectivenessTest")
    public Map ShareCaseEffectivenessTest(){
        DrawDto drawDto=new DrawDto();
        //该活动有分享方案且中奖率为100
        drawDto.setActivityId(Configs.shareCaseActId);
        drawDto.setAuthKey("13928248298");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248298");
        JSONObject res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getIntValue("code")==200){

            JSONObject prize=res.getJSONArray("trophyItems").getJSONObject(0);
            int dayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
            int totalPartTimes = actRedisKeyManage.getUserTotalPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());

            ShareCaseDto shareDto=new ShareCaseDto();
            shareDto.setActivityId(drawDto.getActivityId().toString());
            shareDto.setAuthKey(drawDto.getAuthKey());
            shareDto.setType(drawDto.getType());
            res= HttpUtil.post(Configs.engineBaseURL+Configs.shareURL,shareDto,null);

            JSONArray msgArray=new JSONArray();
            if(res.getIntValue("code")==200){
                //分享成功，检查分享收益
                //先获取分享方案的信息，配置了哪些方案
                List<ShareCaseInfo> shareCases= shareCaseQueryService.queryShareCaseByActId(drawDto.getActivityId());
                if(shareCases!=null) {
                    for (ShareCaseInfo shareCaseInfo : shareCases) {
                        if (shareCaseInfo.workCondition.equals(WorkCondition.ONLY_SHARE)) {
                            if (shareCaseInfo.shareBuffType.equals(ShareBuffType.ADD_JOIN_TIMES)) {
                                //属于加次数的方案, 查询当前用户的剩余抽奖次数
                                //TODO 还需要判断每分享N次才生效，但是现在这个服务没有提供这个N的值，英杰说以后添加
                                //if(){}
                                int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
                                int newTotalPartTimes = actRedisKeyManage.getUserTotalPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
                                if ((dayPartTimes - newDayPartTimes) != shareCaseInfo.shareBuffValue.intValue()) {
                                    String msg="分享增加抽奖次数有bug。活动ID为：" + drawDto.getActivityId() + ",分享前每日已抽奖次数：" + dayPartTimes + ",分享后每日已抽奖次数:" + newDayPartTimes;
                                    msgArray.add(msg);
                                }
                                if ((totalPartTimes - newTotalPartTimes) != shareCaseInfo.shareBuffValue.intValue()) {
                                    String msg="分享活动增加抽奖次数有bug。活动ID为：" + drawDto.getActivityId() + ",分享前总的已抽奖次数：" + dayPartTimes + ",分享后总的已抽奖次数:" + newDayPartTimes;
                                    msgArray.add(msg);
                                }
                            }

                            //属于加收益的方案,一个方案可能有多个trophyId，而且是String类型，格式为'ID1,ID2,ID3'
                            if (shareCaseInfo.shareBuffType.equals(ShareBuffType.ADD_EXTRA_MONEY)) {
                                String trophyIds[]=shareCaseInfo.trophyId.split(",");
                                for(String trophyId:trophyIds){
                                    Long tId=Long.valueOf(trophyId);

                                    if(tId.equals(prize.getLong("trophyId"))){
                                        TrophyItemInfo trophyItemInfo=trophyItemResultService.querySharedRecordByTrophyId(drawDto.getActivityId(),tId,drawDto.getAuthKey());
                                        if (trophyItemInfo==null){
                                            String msg="分享后，没有增加对应的奖品收益。奖品id为："+tId;
                                            msgArray.add(msg);
                                        }else if(!(trophyItemInfo.trophyWorth.equals(shareCaseInfo.shareBuffValue))){
                                            String msg="分享后，增加的收益的值不正确。"+"，分享方案配置的值为："+shareCaseInfo.shareBuffValue+",实际增加的值为："+trophyItemInfo.trophyWorth;
                                            msgArray.add(msg);
                                        }
                                    }else{
                                        if (trophyItemResultService.querySharedRecordByTrophyId(drawDto.getActivityId(),tId,drawDto.getAuthKey())!=null){
                                            String msg="并没有中到该奖品，却有它的分享收益。奖品id为："+tId;
                                            msgArray.add(msg);
                                        }
                                    }
                                }


                            }
                        }
                    }

                    //判断是否存在不属于分享方案的奖品收益
                    List<TrophyItemInfo> shareRecs=trophyItemResultService.querySharedRecords(drawDto.getActivityId(),drawDto.getAuthKey());
                    List<Long> list=new ArrayList<>();
                    for(TrophyItemInfo info:shareRecs){
                        boolean flag=false;
                        for(ShareCaseInfo shareCaseInfo:shareCases){
                            if(shareCaseInfo.shareBuffType.equals(ShareBuffType.ADD_EXTRA_MONEY)){
                                String[] trophyIds=shareCaseInfo.trophyId.split(",");
                                for(String trophyId:trophyIds){
                                    Long tId=Long.valueOf(trophyId);

                                    if(info.trophyId.equals(tId)){
                                        flag=true;
                                    }
                                }
                            }

                        }
                        if(!flag){
                            list.add(info.trophyId);
                        }
                    }

                    if(list.size()!=0){
                        String msg="存在不属于分享方案的奖品收益。奖品id:"+list.toString();
                        msgArray.add(msg);
                    }

                }
                if(msgArray.size()!=0){
                    return responseUtil.sendResult(false,msgArray);
                }else{
                    return responseUtil.sendResult(true,"");
                }

            }else{
                return  responseUtil.sendResult(false,"分享接口出错："+res.getString("msg"));
            }
        }else{
            return responseUtil.sendResult(false,"抽奖接口出错，导致测试无法进行。");
        }

    }

    @RequestMapping(value="/noShareCaseTest")
    //没有分享方案，分享后无收益
    public Map noShareCaseTest(){
        JSONArray msgArray=new JSONArray();
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.userVertifyActId);
        drawDto.setAuthKey("13928248298");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248298");
        JSONObject res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getIntValue("code")==200){
            int dayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
            int totalPartTimes = actRedisKeyManage.getUserTotalPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());

            ShareCaseDto shareDto=new ShareCaseDto();
            shareDto.setActivityId(drawDto.getActivityId().toString());
            shareDto.setAuthKey(drawDto.getAuthKey());
            shareDto.setType(drawDto.getType());
            res= HttpUtil.post(Configs.engineBaseURL+Configs.shareURL,shareDto,null);

            if(res.getIntValue("code")==200){
                int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
                int newTotalPartTimes = actRedisKeyManage.getUserTotalPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());

                if(dayPartTimes!=newDayPartTimes){
                    String msg="没有分享方案，但分享后，每日抽奖次数改变了。分享前"+dayPartTimes+"；分享后："+newDayPartTimes;
                    msgArray.add(msg);
                }
                if(totalPartTimes!=newTotalPartTimes){
                    String msg="没有分享方案，但分享后，总抽奖次数改变了。分享前"+totalPartTimes+"；分享后："+newTotalPartTimes;
                    msgArray.add(msg);
                }

                List<TrophyItemInfo> shareRecs=trophyItemResultService.querySharedRecords(drawDto.getActivityId(),drawDto.getAuthKey());

                if(shareRecs.size()>0){
                    String msg="没有分享方案，但是分享后，却有分享收益。收益信息如下： "+JSONArray.toJSON(shareRecs).toString();
                    msgArray.add(msg);
                }
            }
        }
        if(msgArray.size()!=0){
            return responseUtil.sendResult(false,msgArray);
        }else{
            return responseUtil.sendResult(true,"");
        }

    }

    @RequestMapping(value = "/shareLimitTimesTest")
    public Map shareLimitTimesTest(){
        JSONArray msgArray=new JSONArray();
        DrawDto drawDto=new DrawDto();
        //该活动有分享方案且中奖率为100,且奖品种类只有一个。增加收益的生效次数要小于等于增加抽奖次数的生效次数。生效方式为分享即生效。
        drawDto.setActivityId(Configs.shareCaseActId);
        drawDto.setAuthKey("13928248299");
        drawDto.setType(CredentialType.WECHAT);
        drawDto.setPhone("13928248299");
        JSONObject res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);

        if(res.getIntValue("code")==200){
            int dayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
            int totalPartTimes = actRedisKeyManage.getUserTotalPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());

            List<ShareCaseInfo> shareCases= shareCaseQueryService.queryShareCaseByActId(drawDto.getActivityId());
            int times=0;
            int buffValue=0;
            int Times=0;
            for(ShareCaseInfo info:shareCases){
                if(info.shareBuffType.equals(ShareBuffType.ADD_JOIN_TIMES)){
                    times=info.workTimes;
                    buffValue=info.shareBuffValue.intValue();
                }
                if(info.shareBuffType.equals(ShareBuffType.ADD_EXTRA_MONEY)){
                    Times=info.workTimes;
                }
            }


            List<TrophyItemInfo> shareRecs=trophyItemResultService.querySharedRecords(drawDto.getActivityId(),drawDto.getAuthKey());
            ShareCaseDto shareDto=new ShareCaseDto();
            shareDto.setActivityId(drawDto.getActivityId().toString());
            shareDto.setAuthKey(drawDto.getAuthKey());
            shareDto.setType(drawDto.getType());
            for(int i=0;i<times+1;i++){
                res= HttpUtil.post(Configs.engineBaseURL+Configs.shareURL,shareDto,null);
                if(res.getIntValue("code")!=200){
                    return responseUtil.sendResult(false,"分享接口出错，测试中断。错误信息："+res.toString());
                }
            }

            int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
            int newTotalPartTimes = actRedisKeyManage.getUserTotalPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());

            if((dayPartTimes-newDayPartTimes)!=times*buffValue){
                String msg="分享后，每日抽奖次数增加的次数不正确。分享前次数："+dayPartTimes+",分享后次数："+newDayPartTimes+"，分享后应该增加的次数为："+(times*buffValue);
                msgArray.add(msg);
            }
            if((totalPartTimes-newTotalPartTimes)!=times*buffValue){
                String msg="分享后，总抽奖次数增加的次数不正确。分享前次数："+totalPartTimes+",分享后次数："+newTotalPartTimes+"，分享后应该增加的次数为："+(times*buffValue);
                msgArray.add(msg);
            }
            List<TrophyItemInfo> recs=trophyItemResultService.querySharedRecords(drawDto.getActivityId(),drawDto.getAuthKey());
            int recsSize=0;
            int shareRecsSize=0;
            if(recs!=null){
                recsSize=recs.size();
            }
            if(shareRecs!=null){
                shareRecsSize=shareRecs.size();
            }
            if(recsSize-shareRecsSize!=Times){
                String msg="分享方案的奖品收益次数限制无效。实际生效次数："+(recsSize-shareRecsSize)+",方案配置的次数："+Times;
                msgArray.add(msg);
            }

        }else{
            return responseUtil.sendResult(false,"抽奖接口出错，测试中断。错误信息："+res.toString());
        }

        if(msgArray.size()!=0){
            return responseUtil.sendResult(false,msgArray);
        }else{
            return responseUtil.sendResult(true,"");
        }

    }

    @RequestMapping(value = "/shareCaseEffectiveModeTest")
    public Map shareCaseEffectiveModeTest(){
        JSONArray msgArray=new JSONArray();
        DrawDto drawDto=new DrawDto();
        //这里的活动ID，应该填分享即有效的活动，且有增加次数的分享方案。
        drawDto.setActivityId(Configs.shareCaseActId);
        drawDto.setAuthKey("13928248270");
        drawDto.setType(CredentialType.WECHAT);
        drawDto.setPhone("13928248270");
        JSONObject res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getIntValue("code")==200){
            int dayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());

            List<ShareCaseInfo> shareCases= shareCaseQueryService.queryShareCaseByActId(drawDto.getActivityId());

            int buffValue=0;
            for(ShareCaseInfo info:shareCases){
                if(info.shareBuffType.equals(ShareBuffType.ADD_JOIN_TIMES)){
                    buffValue=info.shareBuffValue.intValue();
                }
            }
            ShareCaseDto shareDto=new ShareCaseDto();
            shareDto.setActivityId(drawDto.getActivityId().toString());
            shareDto.setAuthKey(drawDto.getAuthKey());
            shareDto.setType(drawDto.getType());
            res= HttpUtil.post(Configs.engineBaseURL+Configs.shareURL,shareDto,null);
            if(res.getIntValue("code")==200){
                int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
                if((dayPartTimes-newDayPartTimes)!=buffValue){
                    String msg="分享即生效的生效方式无效。";
                    msgArray.add(msg);
                }
            }

            //此处活动ID用分享后，分享对象成功参与活动的生效方式
            drawDto.setActivityId(Configs.shareCaseModelActId);
            res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
            if(res.getIntValue("code")==200){
                dayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
                shareCases= shareCaseQueryService.queryShareCaseByActId(drawDto.getActivityId());

                buffValue=0;
                for(ShareCaseInfo info:shareCases){
                    if(info.shareBuffType.equals(ShareBuffType.ADD_JOIN_TIMES)){
                        buffValue=info.shareBuffValue.intValue();
                    }
                }


                shareDto.setActivityId(drawDto.getActivityId().toString());
                shareDto.setAuthKey(drawDto.getAuthKey());
                shareDto.setType(drawDto.getType());
                //只分享，方案不生效
                res= HttpUtil.post(Configs.engineBaseURL+Configs.shareURL,shareDto,null);
                if(res.getIntValue("code")==200){
                    int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
                    if(dayPartTimes-newDayPartTimes!=0){
                        String msg="生效方式有问题。配置是需要成功参与游戏，但是实际并不需要。";
                        msgArray.add(msg);
                    }
                }

                //成功参与的含义就是，下一个抽奖者，抽奖时带上fromOpenId和自己的openId
                String fromOpenId=drawDto.getAuthKey();
                drawDto.setFromOpenId(fromOpenId);
                drawDto.setAuthKey("13928248271");
                //英杰说，只能设置为WECHAT类型（注意统一凭证类型）
                drawDto.setType(CredentialType.WECHAT);
                drawDto.setPhone("13928248271");

                res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
                if(res.getInteger("code")==200){
                    int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), fromOpenId, drawDto.getType());
                    if(dayPartTimes-newDayPartTimes!=buffValue){
                        String msg="成功分享且参与活动的生效方式有误。在参与活动后也无生效";
                        msgArray.add(msg);
                    }
                }
                //需求改了，所以下面这段代码先注释掉。（之前是分享后打开页面生效的）
                //调用打开页面接口
//                PassDto passDto=new PassDto();
//                String fromOpenId=drawDto.getAuthKey();
//                passDto.setActivityId(drawDto.getActivityId());
//                passDto.setFromOpenId(fromOpenId);
//                passDto.setNickName("test");
//                passDto.setOpenId("8358d4157c194291816de28943f522a0");
//                passDto.setProvince("广东");
//                res= HttpUtil.post(Configs.engineBaseURL+Configs.passURL+fromOpenId,passDto);
//
//                if(res.getInteger("code")==200){
//                    int newDayPartTimes = actRedisKeyManage.getUserTodayPlayTimes(drawDto.getActivityId(), drawDto.getAuthKey(), drawDto.getType());
//                    if(newDayPartTimes-dayPartTimes!=buffValue){
//                        String msg="分享且打开页面生效方式有问题。打开页面后仍无生效。";
//                        msgArray.add(msg);
//                    }
//                }

            }
        }
        if(msgArray.size()>0){
            return responseUtil.sendResult(false,msgArray);
        }else{
            return responseUtil.sendResult(true,"");
        }
    }

    public static void main(String[] args) {
        String s ="8";
        Long l=Long.getLong(s);
        System.out.println(l);

        Long L= Long.valueOf(s);
        System.out.println(">>>>"+L);
    }
}
