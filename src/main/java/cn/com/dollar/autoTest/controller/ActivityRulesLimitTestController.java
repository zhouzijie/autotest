package cn.com.dollar.autoTest.controller;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.service.implement.AutoTestServiceImp;
import cn.com.dollar.autoTest.service.interfaces.AutoTestService;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.engine.act.group.model.ActGroupInfo;
import cn.com.dollar.engine.act.group.model.type.GroupType;
import cn.com.dollar.engine.act.group.service.IActGroupQueryService;
import cn.com.dollar.engine.act.main.model.ActivityMainInfo;
import cn.com.dollar.engine.act.main.model.type.ActStatus;
import cn.com.dollar.engine.act.main.service.IActOperateService;
import cn.com.dollar.engine.act.parameter.model.type.ParameterKey;
import cn.com.dollar.engine.act.parameter.model.type.ParameterType;
import cn.com.dollar.engine.act.parameter.service.IActParameterOperateService;
import cn.com.dollar.engine.trophy.main.model.GroupRuleInfo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.dollar.autoTest.service.interfaces.ActMainService;
import cn.com.dollar.autoTest.service.interfaces.ActParameterService;
import cn.com.dollar.autoTest.service.interfaces.TrophyItemResultService;
import cn.com.dollar.autoTest.util.ActRedisKeyManage;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.autoTest.util.ResponseUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ZZJ on 2017-2-27.
 */
@RestController
public class ActivityRulesLimitTestController {

    private ResponseUtil responseUtil=new ResponseUtil();
    @Autowired
    TrophyItemResultService trophyItemResultService;
    @Autowired
    ActParameterService actParameterService;
    @Autowired
    IActOperateService actOperateService;
    @Autowired
    ActRedisKeyManage actRedisKeyManage;
    @Autowired
    ActMainService actMainService;
    @Autowired
    IActGroupQueryService actGroupQueryService;
    @Autowired
    IActParameterOperateService actParameterOperateService;

    //每日抽奖次数限制
    @RequestMapping(value = "/dayDrawTimesLimitTest")
    public Map dayDrawTimesLimitTest(){
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.actRulesActId);
        drawDto.setAuthKey("13928248293");
        drawDto.setType(CredentialType.MOBILE);
        drawDto.setPhone("13928248293");
        int dayDrawTimes=actParameterService.getActParameter(new BigDecimal(drawDto.getActivityId().toString()), ParameterKey.PART_DAY_TIMES);
        JSONObject res=null;
        //循环抽奖，次数比限制多一次
        for(int i=0;i<dayDrawTimes+1;i++){
            res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        }
        //如果抽奖成功
        if(res.getInteger("code")==200){
            return responseUtil.sendResult(false,"超出允许的每日抽奖次数");
        }
        return responseUtil.sendResult(true,"");
    }

    //每日中奖次数测试
    @RequestMapping(value = "/dayLotteryTimesLimitTest")
    public Map dayLotteryTimesLimitTest(){
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.actRulesActId);
        drawDto.setAuthKey("13928248294");
        drawDto.setType(CredentialType.WECHAT);
        drawDto.setPhone("13928248294");

        actParameterOperateService.updateActParameterBy(drawDto.getActivityId(), ParameterType.LOTTERY, ParameterKey.LOTTERY_DAY_TIMES,1,new BigDecimal("1"),null,null);
        actParameterOperateService.updateActParameterBy(drawDto.getActivityId(), ParameterType.LOTTERY, ParameterKey.LOTTERY_TOTAL_TIMES,1,new BigDecimal("10"),null,null);
        actParameterOperateService.updateActParameterBy(drawDto.getActivityId(), ParameterType.PART, ParameterKey.PART_DAY_TIMES,1,new BigDecimal("3"),null,null);
        actParameterOperateService.updateActParameterBy(drawDto.getActivityId(), ParameterType.PART, ParameterKey.PART_TOTAL_TIMES,1,new BigDecimal("10"),null,null);
        int dayLotteryTimes=actParameterService.getActParameter(new BigDecimal(drawDto.getActivityId().toString()), ParameterKey.LOTTERY_DAY_TIMES);
        JSONObject res;
        //循环抽奖，次数比中奖次数限制多一次
        for(int i=0;i<dayLotteryTimes+1;i++){
            res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
            if(res.getInteger("code")>=500){
                return responseUtil.sendResult(false,"抽奖接口出错："+res.getString("msg"));
            }
        }
        int times=trophyItemResultService.getLotteryTimesByCredentialValue(drawDto.getActivityId(),drawDto.getAuthKey());
        if(times>dayLotteryTimes){
            return responseUtil.sendResult(false,"中奖次数超出限制");
        }else{
            return responseUtil.sendResult(true,"");
        }
    }

    //参与时间限制测试
    @RequestMapping(value = "/joinTimeLimitTest")
    public Map joinTimeLimitTest(){
        JSONObject res;
        JSONArray retMsg=new JSONArray();
        DrawDto drawDto=new DrawDto();

        drawDto.setActivityId(Configs.actTimeLimitActId);
        drawDto.setAuthKey("13928248295");
        drawDto.setType(CredentialType.WECHAT);
        drawDto.setPhone("13928248295");
        //发布活动
        actOperateService.publishActivity(drawDto.getActivityId());
        res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        //未开始应该返回未开始。
        if(res.getInteger("code")==200){
            String msg="发布活动后，但未开始活动，却能抽奖。";
            retMsg.add(msg);
        }
        else if(res.getInteger("code")>=500){
            String msg="发布活动后抽奖出错："+res.getJSONObject("msg");
            retMsg.add(msg);

        }

        //开始活动
        ActivityMainInfo activityMainInfo=new ActivityMainInfo();
        activityMainInfo.actId=drawDto.getActivityId();
        activityMainInfo.actStatus= ActStatus.GOING;
        actMainService.updateActStatus(activityMainInfo);
        //这里需要等redis的信息更新完，不然会误报。
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        if(res.getInteger("code")!=200){
            String msg="开始活动，但抽奖失败："+res.getJSONObject("msg");
            retMsg.add(msg);
        }

       //暂停活动
        activityMainInfo.actStatus=ActStatus.PAUSE;
        actMainService.updateActStatus(activityMainInfo);
        res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        //暂停活动后，抽奖应该返回403
        if(res.getInteger("code")!=403){
            if(res.getInteger("code")>=500||res.getInteger("code")==404){
                String msg="暂停活动后抽奖出错："+res.getJSONObject("msg");
                retMsg.add(msg);
            }
            if(res.getInteger("code")==200){
                String msg="暂停活动后仍能抽奖";
                retMsg.add(msg);
            }
        }

        //结束活动
        activityMainInfo.actStatus=ActStatus.END;
        actMainService.updateActStatus(activityMainInfo);
        //这里需要等redis的信息更新完，不然会误报。
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        res= HttpUtil.post(Configs.engineBaseURL+Configs.drawURL,drawDto,null);
        //结束活动后，抽奖应该返回403
        if(res.getInteger("code")!=403){
            if(res.getInteger("code")>=500||res.getInteger("code")==404){
                String msg="结束活动后抽奖出错："+res.getJSONObject("msg");
                retMsg.add(msg);
            }
            if(res.getInteger("code")==200){
                String msg="结束活动后仍能抽奖";
                retMsg.add(msg);
            }
        }
        if (retMsg.size() > 0) {
            return responseUtil.sendResult(false,retMsg);
        }else{
            return responseUtil.sendResult(true,"");
        }


    }

    //奖品上限限制测试,该测试需要设置活动的奖品数量小一点。
    //这个测试有个问题是，ip限制的上限需要比并发数大才行。但，ip限制次数大的话，IP测试接口就会循环好多次。
    @RequestMapping(value = "/prizeQuantitylimitTest")
    public Map prizeQuantitylimitTest(){
        //设置并发数
        int concurrent=1000;

        AutoTestService autoTestService=new AutoTestServiceImp();
        autoTestService.ConcurrentDraw(concurrent,Configs.engineBaseURL+Configs.drawURL,Configs.actRulesActId);
        List<ActGroupInfo> groups=actGroupQueryService.getGroups(Configs.actRulesActId);
        Long groupId= 0L;
        for(ActGroupInfo group:groups){
            if(group.groupType.equals(GroupType.DEFAULT))
            groupId=group.groupId;
        }

        List<GroupRuleInfo> infos= actGroupQueryService.getGroupRules(Configs.actRulesActId,groupId,true);
        for(GroupRuleInfo info : infos){

            if(info.trophyInfo.trophyLeft.intValue()!=0){
                return responseUtil.sendResult(false,"奖品剩余量不为0。");
            }
            if(info.trophyInfo.trophyLeft.intValue()<0){
                return responseUtil.sendResult(false,"奖品剩余量为负。");
            }
        }
        return responseUtil.sendResult(true,"");
    }

}
