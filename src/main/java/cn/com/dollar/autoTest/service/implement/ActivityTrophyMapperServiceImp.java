package cn.com.dollar.autoTest.service.implement;

import cn.com.dollar.autoTest.mapper.activityTrophyMapper.ActivityTrophyMapper;
import cn.com.dollar.autoTest.model.ActivityTrophyMapperModel;
import cn.com.dollar.autoTest.model.LeftCount;
import cn.com.dollar.autoTest.service.interfaces.ActivityTrophyMapperService;
import cn.com.dollar.autoTest.service.interfaces.TrophyItemResultService;
import cn.com.dollar.autoTest.util.RedisOperateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZZJ on 2016-12-2.
 */
@Service
public class ActivityTrophyMapperServiceImp implements ActivityTrophyMapperService {
    @Autowired
    ActivityTrophyMapper activityTrophyMapper;
    @Autowired
    TrophyItemResultService trophyItemResultService;
    @Autowired
    RedisOperateUtil redisOperateUtil;

    private List<ActivityTrophyMapperModel> list;

    @Override
    public List<ActivityTrophyMapperModel> getTrophyMessage(BigDecimal actId) {
        return activityTrophyMapper.getTrophyMessage(actId);
    }

    @Override
    public Map<BigDecimal, String> getRedisTrophyLefts(BigDecimal actId) {
        list=getTrophyMessage(actId);
//        Jedis jedis = null;


        Map<BigDecimal,String> trophyLefts=new HashMap<>();
        for(int i=0;i<list.size();i++){
            String key= MessageFormat.format("actId_{0}:groupId_{1}:trophyLeft:trophyId_{2}_leftCount", list.get(i).getActId(),
                    list.get(i).getGroupId(), list.get(i).getTrophyId());
            trophyLefts.put(list.get(i).getTrophyId(),redisOperateUtil.get(key));
        }
        return  trophyLefts;
    }

    @Override
    public Map<BigDecimal,Integer> getTrophyLeft(BigDecimal actId){
        list=getTrophyMessage(actId);
        Map<BigDecimal,Integer> countMap=trophyItemResultService.getTrophyItemCountMap(actId,list);
        Map<BigDecimal,Integer> leftMap=new HashMap<>();

            for(ActivityTrophyMapperModel activityTrophyMapperModel:list){
                Integer left =activityTrophyMapperModel.getQuantityLimit()-countMap.get(activityTrophyMapperModel.getTrophyId());
                leftMap.put(activityTrophyMapperModel.getTrophyId(),left);

        }
        return leftMap;
    }

    @Override
    public Map getTrophyLeftCheck(BigDecimal actId){
        Map<String ,Object> result=new HashMap<>();
       List<LeftCount> unpassList=new ArrayList<>();

        Map<BigDecimal,String> redisMap=getRedisTrophyLefts(actId);
        Map<BigDecimal,Integer> leftMap=getTrophyLeft(actId);
        result.put("status",true);
        for (BigDecimal key : leftMap.keySet()) {
            if(!leftMap.get(key).toString().equals(redisMap.get(key))){
                //redis的剩余和实际计算出来的不相等，把不相等的信息显示到前端
                LeftCount leftCount=new LeftCount();
                result.put("status",false);
                leftCount.setTrophyId(key);
                leftCount.setLeft(leftMap.get(key).toString());
                leftCount.setRedisLeft(redisMap.get(key));
                unpassList.add(leftCount);
            }
        }
        result.put("result",unpassList);
        return  result;
    }
}
