package cn.com.dollar.autoTest.service.interfaces;


import cn.com.dollar.engine.act.parameter.model.type.ParameterKey;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-11-15.
 */
public interface ActParameterService {
    public int getActParameter(BigDecimal actId, ParameterKey parameterKey);
    public void updateActParameter(Long actId,BigDecimal ipLimit,String times);
}
