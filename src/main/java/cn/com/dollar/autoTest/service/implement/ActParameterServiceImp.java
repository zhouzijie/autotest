package cn.com.dollar.autoTest.service.implement;


import cn.com.dollar.autoTest.mapper.actParameter.ActParameterMapper;
import cn.com.dollar.engine.act.parameter.model.type.ParameterKey;
import cn.com.dollar.engine.act.parameter.model.type.ParameterType;
import cn.com.dollar.engine.act.parameter.service.IActParameterOperateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.dollar.autoTest.service.interfaces.ActParameterService;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-11-15.
 */
@Service
public class ActParameterServiceImp implements ActParameterService {
@Autowired
ActParameterMapper actParameterMapper;
    @Autowired
    IActParameterOperateService actParameterOperateService;
    public int getActParameter(BigDecimal actId, ParameterKey parameterKey){
        return  actParameterMapper.getActParameter(actId,parameterKey);
    }
    
    public void updateActParameter(Long actId,BigDecimal ipLimit,String times){
        actParameterOperateService.updateActParameterBy(actId, ParameterType.LOTTERY, ParameterKey.LOTTERY_DAY_TIMES,1,ipLimit.add(new BigDecimal(times)),null,null);
        actParameterOperateService.updateActParameterBy(actId,ParameterType.PART, ParameterKey.PART_DAY_TIMES,1,ipLimit.add(new BigDecimal(times)),null,null);
        actParameterOperateService.updateActParameterBy(actId, ParameterType.LOTTERY, ParameterKey.LOTTERY_TOTAL_TIMES,1,ipLimit.add(new BigDecimal(times)),null,null);
        actParameterOperateService.updateActParameterBy(actId, ParameterType.PART, ParameterKey.PART_TOTAL_TIMES,1,ipLimit.add(new BigDecimal(times)),null,null);
    }
}
