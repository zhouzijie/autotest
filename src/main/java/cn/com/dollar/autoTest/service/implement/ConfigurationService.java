package cn.com.dollar.autoTest.service.implement;

import cn.com.dollar.autoTest.model.ProjectProperties;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;

/**
 * Created by ZZJ on 2017-2-17.
 */
@Service
public class ConfigurationService {

    public static ProjectProperties getEngineConfiguration() {
        BigDecimal ipWarningReqTimes;
        FileInputStream in=null;
        Properties props = new Properties();

        ProjectProperties projectProperties=new ProjectProperties();

        try {
            in = new FileInputStream("F:\\code\\engine\\activity-engine-impl\\src\\main\\resources\\activity-engine.properties");
            props.load(in);
            ipWarningReqTimes=new BigDecimal(props.getProperty("draw.ipWarningReqTimes"));
            projectProperties.setIpWarningReqTimes(ipWarningReqTimes);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return projectProperties;
    }

    public static void main(String[] args) {
        System.out.println(ConfigurationService.getEngineConfiguration().getIpWarningReqTimes());
    }
}
