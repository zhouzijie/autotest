package cn.com.dollar.autoTest.service.implement;

import cn.com.dollar.autoTest.Dto.DrawDto;
import cn.com.dollar.autoTest.service.interfaces.AutoTestService;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ZZJ on 2017-5-3.
 */
public class AutoTestServiceImp implements AutoTestService{
    private final static Logger logger = LogManager.getLogger();
    @Override
    public List<String> ConcurrentDraw(int concurrent,String url,Long actId) {
        List<String> authkeys=new ArrayList<>();
        ExecutorService pool = Executors.newCachedThreadPool();

        //初始化测试数据
        List<String> list=getDataFromFile("C:/Users/ZZJ/Desktop/常用/产品/微信.txt");
        Random random=new Random();
        String s;
        String [] strs;
        for(int i=0;i<concurrent;i++){
            int ran=random.nextInt(217624);
            s=list.get(ran);
            strs=s.split("@");

            DrawDto drawDto=new DrawDto();

//            drawDto.setActivityId(Configs.actRulesActId);
            drawDto.setActivityId(actId);
            drawDto.setAuthKey(strs[0]);
            drawDto.setType(CredentialType.WECHAT);
            drawDto.setPhone(strs[1]);
            authkeys.add(strs[0]);
            pool.execute(new Runnable() {
                @Override
                public void run() {
                    JSONObject res= HttpUtil.post(url,drawDto,null);
                }
            });
        }
        pool.shutdown();
        System.out.println("shutdown()：启动一次顺序关闭，执行以前提交的任务，但不接受新任务。");
        while(true){
            if(pool.isTerminated()){
                System.out.println("所有的子线程都结束了！");
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return authkeys;
    }

    @Override
    public JSONObject ConcurrentDrawOrderly(int concurrent, String url, Long actId,CredentialType type) {
        JSONObject jsonObject=new JSONObject();
        List<String> authkeys=new Vector<>();
        ExecutorService pool = Executors.newCachedThreadPool();
        AtomicInteger num = new AtomicInteger(0);
        //初始化测试数据
        List<String> list=getDataFromFile("C:/Users/ZZJ/Desktop/常用/产品/手机号.txt");
        String s;
        String [] strs;
        for(int i=0;i<concurrent;i++){
            s=list.get(i);
            strs=s.split("@");

            DrawDto drawDto=new DrawDto();
            String authKey=strs[0];
//            drawDto.setActivityId(Configs.actRulesActId);
            drawDto.setActivityId(actId);
            drawDto.setAuthKey(authKey);
            drawDto.setType(type);
            drawDto.setPhone(strs[1]);

            pool.execute(new Runnable() {
                @Override
                public void run() {
                    JSONObject res= HttpUtil.post(url,drawDto,null);
                    if(res.getInteger("code")==200){
                        num.getAndAdd(1);
                        authkeys.add(authKey);
                    }
                }
            });
        }
        pool.shutdown();
        System.out.println("shutdown()：启动一次顺序关闭，执行以前提交的任务，但不接受新任务。");
        while(true){
            if(pool.isTerminated()){
                System.out.println("所有的子线程都结束了！");
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jsonObject.put("authkeys",authkeys);
        jsonObject.put("count",num);
        return jsonObject;
    }

    public List<String> getDataFromFile(String filePath){
        List<String> list=new ArrayList<>();
        InputStreamReader read=null;
        BufferedReader bufferedReader=null;
        try {
//            String filePath="C:\\Users\\ZZJ\\Desktop\\常用\\产品\\微信.txt";
            File file=new File(filePath);
            if(file.isFile() && file.exists()){ //判断文件是否存在
                read = new InputStreamReader(
                        new FileInputStream(file));//考虑到编码格式
                bufferedReader = new BufferedReader(read);
                String lineTxt;

                while((lineTxt = bufferedReader.readLine()) != null){
                    //  System.out.println(lineTxt);
                    list.add(lineTxt);

                }
            }else{
                logger.info("找不到指定的文件");
            }
        } catch (Exception e) {
            logger.info("读取文件内容出错");
            e.printStackTrace();
        }finally {
            try {
                if(read!=null){
                    read.close();
                }
                if(bufferedReader!=null){
                    bufferedReader.close();
                }
            }catch (Exception e2){
                e2.printStackTrace();
            }
        }
        return list;
    }

    public static void main(String[] args) {
        AtomicInteger num = new AtomicInteger(0);
        List<String> list=new ArrayList<>();
        list.add("a");
        list.add("b");

        JSONObject o=new JSONObject();
        o.put("list",list);
        o.put("num",num);

        JSONArray array=o.getJSONArray("list");
        System.out.println(array);
        System.out.println(array.getString(0));
        System.out.println(o.getInteger("num"));
    }
}
