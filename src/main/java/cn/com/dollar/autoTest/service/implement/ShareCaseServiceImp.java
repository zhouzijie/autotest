package cn.com.dollar.autoTest.service.implement;

import cn.com.dollar.autoTest.model.ShareCase;
import cn.com.dollar.autoTest.service.interfaces.ShareCaseService;
import cn.com.dollar.autoTest.mapper.shareCase.ShareCaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by ZZJ on 2016-11-24.
 */
@Service
public class ShareCaseServiceImp implements ShareCaseService {
    @Autowired
    ShareCaseMapper shareCaseMapper;
    @Override
    public List<ShareCase> getShareCaseInfo(BigDecimal actId) {
        return shareCaseMapper.getShareCaseInfo(actId);
    }
}
