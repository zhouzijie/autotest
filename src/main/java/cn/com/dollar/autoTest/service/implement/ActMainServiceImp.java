package cn.com.dollar.autoTest.service.implement;

import cn.com.dollar.autoTest.mapper.labels.CLabelsMapper;
import cn.com.dollar.autoTest.mapper.labels.LabelCustomerMapper;
import cn.com.dollar.autoTest.mapper.trophyItem.TrophyItemResultMapper;
import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.autoTest.util.HttpUtil;
import cn.com.dollar.engine.act.main.model.ActivityMainInfo;
import cn.com.dollar.engine.act.main.model.type.ActStatus;
import cn.com.dollar.engine.act.main.service.IActOperateService;
import cn.com.dollar.engine.act.main.service.IActQueryService;
import cn.com.dollar.autoTest.mapper.actMain.ActMainMapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.dollar.autoTest.service.interfaces.ActMainService;
import cn.com.dollar.autoTest.util.ActRedisKeyManage;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by ZZJ on 2016-11-15.
 */
@Service
public class ActMainServiceImp implements ActMainService {
    @Autowired
    LabelCustomerMapper labelCustomerMapper;
    @Autowired
    CLabelsMapper cLabelsMapper;
    @Autowired
    TrophyItemResultMapper trophyItemResultMapper;
    @Autowired
    ActMainMapper actMainMapper;
    @Autowired
    ActRedisKeyManage actRedisKeyManage;
    @Autowired
    IActQueryService actQueryService;
    @Autowired
    IActOperateService actOperateService;
    @Override
    public Date getActEndDate(BigDecimal actId) {
        return actMainMapper.getEndDate(actId);
    }

    @Override
    public int updateActStatus(ActivityMainInfo activityMainInfo) {
        if (activityMainInfo.actStatus.equals(ActStatus.END)) {
            activityMainInfo.endDate=new Date();
        }

        if(activityMainInfo.actStatus.equals(ActStatus.GOING)){
            ActivityMainInfo actMsg=actQueryService.queryActivityMainById(activityMainInfo.actId);

            activityMainInfo.startDate=new Date();
            //如果本来的结束时间比开始时间早，修改为现在的开始时间+10天
            if(actMsg.endDate.before(activityMainInfo.startDate)){
                Calendar calendar   =   new GregorianCalendar();
                calendar.setTime(activityMainInfo.startDate);
                calendar.add(Calendar.DATE,10);

                activityMainInfo.endDate=calendar.getTime();

            }
        }
        if(activityMainInfo.actStatus.equals(ActStatus.PAUSE)){
            activityMainInfo.stopDate=new Date();
        }
        if(activityMainInfo.actStatus.equals(ActStatus.PUBLISH)){
            activityMainInfo.publishDate=new Date();
        }

        int i =actMainMapper.updateAct(activityMainInfo);
        //清除并更新活动配置缓存
        actOperateService.cleanActConfigCache(activityMainInfo.actId);

        return i;
    }

    @Override
    public int updateAct(ActivityMainInfo activityMainInfo) {
        return actMainMapper.updateAct(activityMainInfo);
    }

    @Override
    @Transactional
    public boolean deleteCreateActivityTestData() {
        try{
            List<ActivityMainInfo> mainList=actMainMapper.getActByName("修改活动名字");
            List<Long> ids=new ArrayList<>();
            for(ActivityMainInfo info : mainList){
                ids.add(info.actId);
                trophyItemResultMapper.dropTableByName("trophy_item_"+info.actId);
            }
            if(ids.size()>0){
                actMainMapper.deleteActByIds(ids);
            }
            List<Long> labelIds=cLabelsMapper.getLabelIdsByName("c_label_"+Configs.companyId,"测试标签A");
            if(labelIds!=null&&labelIds.size()>0){
                labelCustomerMapper.deleteByIds("label_customer_"+Configs.companyId,labelIds);
                cLabelsMapper.deleteLabelsByIds("c_label_"+Configs.companyId,labelIds);
            }

            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean deletePageViewTestData(String token) {
        ActivityMainInfo activityMainInfo=new ActivityMainInfo();
        activityMainInfo.actStatus= ActStatus.UN_PUBLISH;
        activityMainInfo.startDate=new Date(127,9,9);
        activityMainInfo.endDate=new Date(230,8,8);
        activityMainInfo.actId=Configs.pageViewActId;
        updateAct(activityMainInfo);

        //更新redis缓存
        actOperateService.cleanActConfigCache(activityMainInfo.actId);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //删除数据
        JSONObject res= HttpUtil.delete(Configs.portalBaseURL+Configs.cleanTestDataURL+"/"+Configs.pageViewActId,token);
        if(res.getInteger("code")!=200){
            return false;
        }

        //更新为进行中，因为统计数据要在活动开始后才会被计算。
        activityMainInfo.actStatus=ActStatus.GOING;
        activityMainInfo.startDate=new Date();
        activityMainInfo.actId=Configs.pageViewActId;
        updateAct(activityMainInfo);

        //更新redis缓存
        actOperateService.cleanActConfigCache(activityMainInfo.actId);
        return true;
    }

    public static JSONObject getLoginMessage() throws UnsupportedEncodingException {
        String client_id="web";
        String client_secret="3vlGiHMJaKgnYqu09Qg4CB2G0JtbyAe6";
        String grant_type="password";
        String scope= URLEncoder.encode("read write","utf-8");
        String username="AutoTest";
        String password="szl07302";
        return HttpUtil.post(Configs.authzBaseURL+Configs.loginURL+"?client_id="+client_id+"&client_secret="+client_secret+
                "&grant_type="+grant_type+"&scope="+scope+"&username="+username+"&password="+password,null,null);

    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        JSONObject res=ActMainServiceImp.getLoginMessage();
        System.out.print(res.toString());
    }
}
