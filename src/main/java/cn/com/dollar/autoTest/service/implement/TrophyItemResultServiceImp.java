package cn.com.dollar.autoTest.service.implement;

import cn.com.dollar.autoTest.mapper.trophyItem.TrophyItemResultMapper;
import cn.com.dollar.autoTest.model.ShareCase;
import cn.com.dollar.autoTest.service.interfaces.ShareCaseService;
import cn.com.dollar.autoTest.service.interfaces.TrophyItemResultService;
import cn.com.dollar.engine.trophy.item.model.TrophyItem;
import cn.com.dollar.engine.trophy.item.model.TrophyItemInfo;
import cn.com.dollar.autoTest.model.ActivityTrophyMapperModel;
import cn.com.dollar.autoTest.model.TrophyItemCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ZZJ on 2016-11-10.
 */
@Service
public class TrophyItemResultServiceImp implements TrophyItemResultService {
    @Autowired
    TrophyItemResultMapper trophyItemResultMapper;
    @Autowired
    ShareCaseService shareCaseService;

    public List<Integer> drawTimesCheck(BigDecimal actId,int daysDrawTimes) {
        return trophyItemResultMapper.drawTimesCheck(actId,daysDrawTimes);
    }

    public List<Integer> lotteryTimesCheck(BigDecimal actId,int dayLotteryTimes) {
        return trophyItemResultMapper.lotteryTimesCheck(actId,dayLotteryTimes);
    }

    public List<String> callerIpCheck(BigDecimal actId) {
        return trophyItemResultMapper.callerIpCheck(actId);
    }

    public List<Integer> credentialWithPhones(BigDecimal actId) {
        return trophyItemResultMapper.credentialWithPhones(actId);
    }

    public List<String> phoneWithCredential(BigDecimal actId) {
        return trophyItemResultMapper.phoneWithCredential(actId);
    }

    public List<Integer> overTimeItems(BigDecimal actId,Date endDate) {
        return trophyItemResultMapper.overTimeItems(actId,endDate);
    }

    @Override
    public List<String> getIllegalPhone(BigDecimal actId) {
        List<String> phones=trophyItemResultMapper.getPhones(actId);
        Pattern p=Pattern.compile("^1[3|4|5|7|8]\\d{9}$");
        List<String> illegalPhones=new ArrayList<>();
        for(int i=0;i<phones.size();i++){
            Matcher m=p.matcher(phones.get(i));
            if(!m.matches()){
                illegalPhones.add(phones.get(i));
            }
        }
        return illegalPhones;
    }

    @Override
    //根据分享方案添加抽奖/中奖次数上限
    public int getGameTimes(BigDecimal actId,int gameTimes,boolean shareAndOpen,String shareBuffType) {
        List<ShareCase> shareCaseInfo=shareCaseService.getShareCaseInfo(actId);
        if (shareCaseInfo.size()>0){
            boolean condition;
            for(int i=0;i<shareCaseInfo.size();i++){
                //shareAndOpen的作用是标志脚本是否有实现打开分享后的链接。true为实现了，false为没实现。
                if(shareAndOpen){
                    condition=shareCaseInfo.get(i).getShareBuffType().equals(shareBuffType);
                }else{
                    condition=shareCaseInfo.get(i).getWorkCondition().equals("ONLY_SHARE")&&shareCaseInfo.get(i).getShareBuffType().equals(shareBuffType);
                }
                if(condition){
                    for(int j=0;j<shareCaseInfo.get(i).getWorkTimes();j++){
                        gameTimes+=shareCaseInfo.get(i).getShareBuffValue();
                    }
                }
            }
        }else{
        System.out.println("活动"+actId+"，没有分享方案");
        }
        return  gameTimes;
    }

    @Override
    public List<TrophyItemCount> getTrophyItemCount(BigDecimal actId) {
        return trophyItemResultMapper.getTrophyItemCount(actId);
    }

    @Override
    public Map<BigDecimal,Integer> getTrophyItemCountMap(BigDecimal actId, List<ActivityTrophyMapperModel> list){
        Map<BigDecimal,Integer> map=new HashMap<BigDecimal,Integer>();
        for(ActivityTrophyMapperModel activityTrophyMapperModel:list){
            Integer count=trophyItemResultMapper.queryTrophyCount(actId,activityTrophyMapperModel.getTrophyId());
                map.put(activityTrophyMapperModel.getTrophyId(),count);
        }
        return map;
    }

    @Override
    public int getLotteryTimesByCredentialValue(Long actId,String credentialValue) {
        return trophyItemResultMapper.getLotteryCount(actId,credentialValue);

    }

    @Override
    public List<TrophyItem> getTrophyItemByPhone(Long actId, String phone) {
        return trophyItemResultMapper.getTrophyItemByPhone(actId,phone);
    }

    @Override
    public List<TrophyItem> getTrophyItemByCredential(Long actId, String credentialValue) {
        return trophyItemResultMapper.getTrophyItemByCredentialValue(actId,credentialValue);
    }

    @Override
    public List<TrophyItemInfo> querySharedRecords(Long actId,String credentialValue) {
        return trophyItemResultMapper.querySharedRecords(actId ,credentialValue);
    }

    @Override
    public TrophyItemInfo querySharedRecordByTrophyId(Long actId,Long trophyId,String credentialValue) {
        return trophyItemResultMapper.querySharedRecordByTrophyId(actId,trophyId,credentialValue);
    }

    public static void main(String[] args) {
        List<String> phones=new ArrayList<>();
        phones.add("12345678901");
        phones.add("1345655");
        phones.add("13928248282");
        Pattern p=Pattern.compile("^1[3|4|5|7|8]\\d{9}$");
        List<String> illegalPhones=new ArrayList<>();
        for(int i=0;i<phones.size();i++){
            Matcher m=p.matcher(phones.get(i));
            if(!m.matches()){
                illegalPhones.add(phones.get(i));
                System.out.println(phones.get(i));
            }
        }
        System.out.println(illegalPhones.toString());
    }

}
