package cn.com.dollar.autoTest.service.interfaces;

import cn.com.dollar.autoTest.model.ActivityTrophyMapperModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by ZZJ on 2016-12-2.
 */
public interface ActivityTrophyMapperService {
    public List<ActivityTrophyMapperModel> getTrophyMessage(BigDecimal actId);
    public Map<BigDecimal,String> getRedisTrophyLefts(BigDecimal actId);
    public Map<BigDecimal,Integer> getTrophyLeft(BigDecimal actId);
    public Map getTrophyLeftCheck(BigDecimal actId);

}
