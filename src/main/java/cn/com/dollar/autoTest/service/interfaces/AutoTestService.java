package cn.com.dollar.autoTest.service.interfaces;

import cn.com.dollar.customer.label.person.model.type.CredentialType;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by ZZJ on 2017-5-3.
 */
public interface AutoTestService{
    public List<String> ConcurrentDraw(int concurrent, String url, Long actId);
    public JSONObject ConcurrentDrawOrderly(int concurrent, String url, Long actId, CredentialType type);
}
