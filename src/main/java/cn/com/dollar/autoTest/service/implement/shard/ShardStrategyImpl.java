package cn.com.dollar.autoTest.service.implement.shard;

import cn.com.dollar.engine.trophy.item.model.TrophyItem;
import cn.com.dollar.engine.trophy.item.model.TrophyItemInfo;
import com.google.code.shardbatis.strategy.ShardStrategy;

import java.util.HashMap;

/**
 * Created by zyj on 2016/12/5.
 */
public class ShardStrategyImpl implements ShardStrategy {

    @Override
    /**
     * 得到实际表名
     * @param baseTableName 逻辑表名,一般是没有前缀或者是后缀的表名
     * @param params mybatis执行某个statement时使用的参数
     * @param mapperId mybatis配置的statement id
     * @return
     */
    public String getTargetTableName(String tableName, Object params, String mapperId) {
        String sTableName = tableName;
        if (params instanceof HashMap) {
            Object actId = ((HashMap) params).get("actId");
            sTableName += "_" + actId;
        } else if (params instanceof TrophyItem) {
            sTableName += "_" + ((TrophyItem) params).getActId();
        } else if (params instanceof TrophyItemInfo) {
            sTableName += "_" + ((TrophyItemInfo) params).actId;
        } else if (params instanceof Long) {
            sTableName += "_" + params;
        }

        return sTableName;
    }
}
