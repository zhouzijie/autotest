package cn.com.dollar.autoTest.service.interfaces;

import cn.com.dollar.engine.trophy.item.model.TrophyItem;
import cn.com.dollar.engine.trophy.item.model.TrophyItemInfo;
import cn.com.dollar.autoTest.model.ActivityTrophyMapperModel;
import cn.com.dollar.autoTest.model.TrophyItemCount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by ZZJ on 2016-11-10.
 */
public interface TrophyItemResultService {
    public  List<Integer>  drawTimesCheck(BigDecimal actId, int dayDrawTimes);
    public  List<Integer>  lotteryTimesCheck(BigDecimal actId, int dayLotteryTimes);
    public  List<String> callerIpCheck(BigDecimal actId);
    public  List<Integer>  credentialWithPhones(BigDecimal actId);
    public  List<String>  phoneWithCredential(BigDecimal actId);
    public  List<Integer>  overTimeItems(BigDecimal actId, Date endDate);
    public  List<String>  getIllegalPhone(BigDecimal actId);

    public int getGameTimes(BigDecimal actId, int gameTimes, boolean shareAndOpen, String shareBuffType);

    public List<TrophyItemCount> getTrophyItemCount(BigDecimal actId);
    public Map<BigDecimal,Integer> getTrophyItemCountMap(BigDecimal actId, List<ActivityTrophyMapperModel> list);

    public int getLotteryTimesByCredentialValue(Long actId, String credentialValue);
    public List<TrophyItem> getTrophyItemByPhone(Long actId, String phone);
    public List<TrophyItem> getTrophyItemByCredential(Long actId, String credentialValue);

    List<TrophyItemInfo> querySharedRecords(Long actId,String credentialValue);
    public TrophyItemInfo querySharedRecordByTrophyId(Long actId,Long trophyId,String credentialValue);
}
