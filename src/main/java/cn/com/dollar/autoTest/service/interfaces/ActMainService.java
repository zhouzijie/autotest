package cn.com.dollar.autoTest.service.interfaces;

import cn.com.dollar.engine.act.main.model.ActivityMainInfo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ZZJ on 2016-11-15.
 */
public interface ActMainService {
    public Date getActEndDate(BigDecimal actId);
    public int updateActStatus(ActivityMainInfo activityMainInfo);
    public int updateAct(ActivityMainInfo activityMainInfo);

    public boolean deleteCreateActivityTestData();
    public boolean deletePageViewTestData(String token);
}
