package cn.com.dollar.autoTest.util.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Enumeration;
import java.util.Properties;

/**
 *
 * @author zyj
 * @since version
 */
public class PropertyPlaceholderConfigurerExt extends PropertyPlaceholderConfigurer {
    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
            throws BeansException {
        String encrpytkey=props.getProperty("encrpytkey");
        Enumeration en = props.propertyNames();
        while (en.hasMoreElements()) {
            String key = (String) en.nextElement();
            String property = props.getProperty(key);
            if(property.startsWith("encrypt_")){
                property = getBlowFishDecrypted(encrpytkey,property.replaceFirst("encrypt_",""));
                props.setProperty(key,property);
            }
        }

        super.processProperties(beanFactory, props);
    }

    private static final String BLOWFISH_ENCODING = "UTF-8";

    /**
     * BlowFish解密
     *
     * @param key       约定密码
     * @param encrypted 加密的byte数组
     * @return 把字符串解密byte数组
     */
    public static String getBlowFishDecrypted(String key, String encrypted) {
        try {
            SecretKeySpec sksSpec = new SecretKeySpec(key.getBytes(), "Blowfish");
            // BlowFish的加密mode是ECB、Padding方式是PKCS5Padding。
            Cipher cipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, sksSpec);
            byte[] decrypted = cipher.doFinal(stringToBytes(encrypted));
            return new String(decrypted, BLOWFISH_ENCODING);
        } catch (Exception e) {
            throw new IllegalStateException("[Class:EncryptUtils.getBlowFishDecrypt], 摘要算法不被系统支持");
        }
    }

    private static byte[] stringToBytes(String string) {

        byte[] bytes = new byte[string.length() / 2];
        String b;

        for (int i = 0; i < string.length() / 2; i++) {
            b = string.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(b, 16);
        }

        return bytes;
    }
}