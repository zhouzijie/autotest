package cn.com.dollar.autoTest.util;

import cn.com.dollar.customer.label.model.LabelInfo;
import cn.com.dollar.customer.label.person.model.CustomerInfo;
import cn.com.dollar.customer.label.person.model.type.CredentialType;
import cn.com.dollar.customer.label.person.service.ICustomerQueryService;
import cn.com.dollar.customer.label.service.ILabelQueryService;
import cn.com.dollar.engine.act.group.model.ActGroupInfo;
import cn.com.dollar.engine.act.group.service.IActGroupQueryService;
import cn.com.dollar.engine.act.main.service.IActQueryService;
import cn.com.dollar.engine.act.parameter.service.IActParameterQueryService;
import cn.com.dollar.engine.act.share.model.ShareCaseInfo;
import cn.com.dollar.engine.act.share.service.IShareCaseQueryService;
import cn.com.dollar.engine.act.share.type.ShareBuffType;
import cn.com.dollar.engine.trophy.item.model.DrawInfo;
import cn.com.dollar.engine.trophy.item.service.ITrophyItemQueryService;
import cn.com.dollar.engine.trophy.main.model.GroupRuleInfo;
import cn.com.dollar.engine.trophy.main.service.ITrophyQueryService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@Component
public class ActRedisKeyManage {
    @Autowired
    public RedisOperateUtil redisOperateUtil;

    @Autowired
    ITrophyQueryService trophyQueryService;

    @Autowired
    IActQueryService actQueryService;

    @Autowired
    ITrophyItemQueryService trophyItemService;

    @Autowired
    IActParameterQueryService actParameterQueryService;

//    @Autowired
//    StatisticsRedisKeyManager statisticsRedisKeyManager;
//
//    @Autowired
//    UserBehaviorTotalDataRedisCache userBehaviorTotalDataRedisCache;

    @Autowired
    IActGroupQueryService actGroupQueryService;

    /**
     * 奖品明细持久化队列
     */
    public final static String itemQueueName = "itemQueue";
    /**
     * 奖品明细持久化队列（备份）
     */
    public final static String backupItemQueueName = "backupItemQueue";


    public final static String credentialQueueName = "credentialQueue";
    /**
     * 用户凭证持久化队列（备份）
     */
    public final static String backupCredentialQueueName = "backupCredentialQueue";


    //用户每日参与次数
    private final String dayPlayTimes = "actId_{0}:todayPartCount:authKey_{1}_type_{2}_time_{3}_todayCount";
    //参与次数匹配
    private final String dayPlayTimesSearchKey = "actId_{0}:todayPartCount*";

    //用户每日中奖次数
    private final String dayLotteryTimes = "actId_{0}:todayLotteryCount:authKey_{1}_type_{2}_time_{3}_todayCount";
    //中奖次数匹配
    private final String dayLotteryTimesSearchKey = "actId_{0}:todayLotteryCount*";

    //用户总参与次数
    private final String totalPlayTimes = "actId_{0}:totalPartCount:openId_{1}_type_{2}_totalCount";
    //用户总参与次数匹配
    private final String totalPlayTimesSearchKey = "actId_{0}:totalPartCount*";

    //用户总中奖次数
    private final String totalLotteryTimes = "actId_{0}:totalLotteryCount:openId_{1}_type_{2}_totalCount";
    //用户总中奖次数匹配
    private final String totalLotteryTimesSearchKey = "actId_{0}:totalLotteryCount*";

    //奖品剩余数量
    private final String trophyLeftCount = "actId_{0}:groupId_{1}:trophyLeft:trophyId_{2}_leftCount";

    //奖品剩余数量匹配
    private final String trophyLeftSearchKey = "actId_{0}:groupId_*:trophyLeft*";

    private final String trophyGroupCache = "actId_{0}:groupId_{1}:list";

    //存储授权通过的openId
    private final String validateOpenId = "validateOpenIdSet:{0}";

    //活动规则
    private final String actRuleInfo = "actId_{0}:rule";

    //火炬
    private final String torchRankKey = "actId_{0}:torchRank";
    //火炬手人数
    private final String playerKey = "actId_{0}:playerKey";

    //子节点人数subChildren
    private final String subChildren = "actId_{0}:nodes:{1}_subChildren";

    private final String lastUpdateNodeId = "nodes:lastUpdateNodeId";

    //奖品轮播
    private final String actDisplayList = "actId_{0}:displayList";


    //分享次数
    private final String shareTimes = "actId_{0}:share:{1}_{2}";

    private final String shareTimesSearchKey = "actId_{0}:share:*";

    //收集流量套餐
    private final String phoneFlowPlan = "phoneFlowPlanKey";

    //验证码
    private final String verifyCodeKey = "verifyCode:{0}_{1}_{2}";

    //IP请求次数
    private final String ipCounts = "actId_{0}:ipTimes:{1}";

    private final String ipCountsSearchKey = "actId_{0}:ipTimes:*";

    //IP请求限制
    private final String ipReqTimesLimit = "actId_{0}:ipTimesLimit";

    //发送短信验证码间隔时间
    private final int smsVerifyCodeTimeInterval = 60;

//    //每个活动OPENID的PV数统计
//
//    private final String openIdPvKey = "actId_{0}:openId_pv";


    //中奖手机号标记
    private final String markPhoneLotteryKey = "actId_{0}:phone:{1}";


    //通过分享获得增益的次数
    private final String addShareCaseTimes = "actId_{0}:shareCase_{1}:{2}_{3}_{4}";

    private final String addShareCaseTimesSearchKey = "actId_{0}:shareCase*";


    private final String lockKey = "actId_{0}_groupId_{1}";

    private final String lockKeySearch = "actId_{0}_groupId_*";


    private String getDatePatternString() {
        DateTime dt = DateTime.now();
        DateTimeFormatter dateFormat = DateTimeFormat.forPattern("yyyyMMdd");
        return dateFormat.print(dt);
    }

    /**
     * 获取用户当日抽奖次数
     */
    public int getUserTodayPlayTimes(Long actId, String authKey, CredentialType type) {

        String playCountKey = MessageFormat.format(dayPlayTimes, actId, authKey, type, getDatePatternString());
        String userTodayPlayCountValue = redisOperateUtil.get(playCountKey);
        if (userTodayPlayCountValue == null) {
            redisOperateUtil.set(playCountKey, "0");
            return 0;
        } else {
            return Integer.parseInt(userTodayPlayCountValue);
        }
    }

    /**
     * 获取用户当日中奖次数
     */
    public int getUserTodayLotteryTimes(Long actId, String authKey, CredentialType type) {
        String playCountKey = MessageFormat.format(dayLotteryTimes, actId, authKey, type, getDatePatternString());
        String userTodayLotteryCountValue = redisOperateUtil.get(playCountKey);
        if (userTodayLotteryCountValue == null) {
            redisOperateUtil.set(playCountKey, "0");
            return 0;
        } else {
            return Integer.parseInt(userTodayLotteryCountValue);
        }
    }


    /**
     * 获取用户总抽奖次数
     */
    public int getUserTotalPlayTimes(Long actId, String authKey, CredentialType type) {
        String totalPlayCountKey = MessageFormat.format(totalPlayTimes, actId, authKey, type);
        String userTotalPlayCountValue = redisOperateUtil.get(totalPlayCountKey);
        if (userTotalPlayCountValue == null) {
            redisOperateUtil.set(totalPlayCountKey, "0");
            return 0;
        } else {
            return Integer.parseInt(userTotalPlayCountValue);
        }
    }

    /**
     * 获取用户总中奖次数
     */
    public int getUserTotalLotteryTimes(Long actId, String authKey, CredentialType type) {
        String totalPlayCountKey = MessageFormat.format(totalLotteryTimes, actId, authKey, type);
        String userTotalLotteryCountValue = redisOperateUtil.get(totalPlayCountKey);
        if (userTotalLotteryCountValue == null) {
            redisOperateUtil.set(totalPlayCountKey, "0");
            return 0;
        } else {
            return Integer.parseInt(userTotalLotteryCountValue);
        }
    }

    /**
     * 用户当日抽奖次数加1,总次数加1
     */
    public void incrUserPlayTimes(Long actId, String authKey, CredentialType type) {
        //当日加1
        String todayPlayCountKey = MessageFormat.format(dayPlayTimes, actId, authKey, type, getDatePatternString());
        //所以总次数也应该加1
        String totalPlayCountKey = MessageFormat.format(totalPlayTimes, actId, authKey, type);
        redisOperateUtil.incr(totalPlayCountKey);
        redisOperateUtil.incr(todayPlayCountKey);
    }

    /**
     * 用户当日抽奖次数减n次,总次数减n次
     */
    public void decrUserPlayTimes(Long actId, String authKey, CredentialType type, Integer value) {

        requireNonNull(actId);
        requireNonNull(authKey);
        requireNonNull(type);
        requireNonNull(value);
        //当日加1
        String todayPlayCountKey = MessageFormat.format(dayPlayTimes, actId, authKey, type, getDatePatternString());
        //所以总次数也应该加1
        String totalPlayCountKey = MessageFormat.format(totalPlayTimes, actId, authKey, type);

        String todayPlayCount = redisOperateUtil.get(todayPlayCountKey);

        String totalPlayCount = redisOperateUtil.get(totalPlayCountKey);

        if (todayPlayCount == null) {
            redisOperateUtil.set(todayPlayCountKey, "0");
        }
        if (totalPlayCount == null) {
            redisOperateUtil.set(totalPlayCountKey, "0");
        }

        redisOperateUtil.decrBy(todayPlayCountKey, value);
        redisOperateUtil.decrBy(totalPlayCountKey, value);
    }


    /**
     * 用户当日中奖次数加1,总次数加1
     */
    public void incrUserLotteryTimes(Long actId, String authKey, CredentialType type) {
        DateTime dt = DateTime.now();
        DateTimeFormatter dateFormat = DateTimeFormat.forPattern("yyyyMMdd");
        String todayString = dateFormat.print(dt);
        //当日加1
        String todayLotteryCountKey = MessageFormat.format(dayLotteryTimes, actId, authKey, type, getDatePatternString());
        //所以总次数也应该加1
        String totalLotteryCountKey = MessageFormat.format(totalLotteryTimes, actId, authKey, type);
        redisOperateUtil.incr(totalLotteryCountKey);
        redisOperateUtil.incr(todayLotteryCountKey);
    }

    /**
     * 获取某个活动，某个分组下该奖项物品的剩余数量
     */
    public long getTrophyLeftCount(Long actId, Long groupId, Long trophyId) {
        String trophyLeftCountKey = MessageFormat.format(trophyLeftCount, actId, groupId, trophyId);
        long size = redisOperateUtil.llen(trophyLeftCountKey);
        return size;
    }


    /**
     * 将某个活动，某个分组下该奖项物品的剩余数量减1
     */
    public boolean decrTrophyLeftCount(Long actId, Long groupId, Long trophyId) {
        String trophyLeftCountKey = MessageFormat.format(trophyLeftCount, actId, groupId, trophyId);
        if (redisOperateUtil.brpop(trophyLeftCountKey) == null) {
            return false;
        }
        return true;
    }

    /**
     * 将某个活动，某个分组下该奖项物品的剩余数量加1
     */
    public void incrTrophyLeftCount(Long actId, Long groupId, Long trophyId) {
        String trophyLeftCountKey = MessageFormat.format(trophyLeftCount, actId, groupId, trophyId);
        redisOperateUtil.rpush("1");
    }

    /**
     * 设置某个活动，某个分组下该奖项物品的剩余数量
     */
    public void setTrophyLeftCount(Long actId, Long groupId, Long trophyId, BigDecimal count) {
        String trophyLeftCountKey = MessageFormat.format(trophyLeftCount, actId, groupId, trophyId);
//        redisOperateUtil.deleteKey(trophyLeftCountKey);
        long left = redisOperateUtil.llen(trophyLeftCountKey);
        long num = count.longValue() - left;
        if (num > 0) {
            String[] numList = new String[(int) num];
            for (int i = 0; i < num; i++) {
                numList[i] = "1";
            }
            redisOperateUtil.rpush(trophyLeftCountKey, numList);
        } else {
            redisOperateUtil.brpop(trophyLeftCountKey, -num);
        }
    }

    /**
     * 读取奖品列表，如果有缓存则读缓存，否则查询数据库
     *
     * @param actId      活动ID
     * @param groupId    组别ID
     * @param canLottery 能否中奖
     * @return
     */
    public List<GroupRuleInfo> getTrophyFromCache(Long actId, Long groupId, boolean canLottery) {
        String key = MessageFormat.format(trophyGroupCache, actId, groupId);
        String value = redisOperateUtil.get(key);
        List<GroupRuleInfo> groupRuleInfos;
        if (value == null) {
            groupRuleInfos = new ArrayList<>();
        } else {
            groupRuleInfos = JSON.parseArray(value, GroupRuleInfo.class);
            if (canLottery) {
                groupRuleInfos = groupRuleInfos
                        .stream()
                        .filter(
                                info ->
                                        (info.weight.compareTo(BigDecimal.ZERO) > 0
                                                && getTrophyLeftCount(actId, groupId, info.trophyId) > 0)
                        ).collect(Collectors.toList());
                BigDecimal left = new BigDecimal(100);

                for (GroupRuleInfo info : groupRuleInfos) {
                    left = left.subtract(info.getWeight());
                }
                if (left.compareTo(BigDecimal.ZERO) > 0) {
                    groupRuleInfos.add(GroupRuleInfo.createThankYouTrophy(actId, left));
                }
            } else {
                groupRuleInfos.clear();
                groupRuleInfos.add(GroupRuleInfo.createThankYouTrophy(actId, new BigDecimal(100)));
            }
        }

        return groupRuleInfos;
    }

    void initTrophyCache(Long actId) {
        List<ActGroupInfo> groupInfos = actGroupQueryService.getGroups(actId);
        for (ActGroupInfo info : groupInfos) {
            Long groupId = info.groupId;
            List<GroupRuleInfo> rules = actGroupQueryService.getGroupRules(actId, groupId, true);

            //初始化奖品缓存
            String trophyCacheKey = MessageFormat.format(trophyGroupCache, actId, groupId);
            redisOperateUtil.set(trophyCacheKey, JSONArray.toJSONString(rules, SerializerFeature.DisableCircularReferenceDetect));
            //初始化剩余数量队列
            for (GroupRuleInfo rule : rules) {
                Long trophyId = rule.getTrophyId();
                BigDecimal count = trophyItemService.queryTrophyCount(actId, trophyId);
                setTrophyLeftCount(actId, groupId, trophyId, rule.trophyInfo.trophyLimit.subtract(count));
            }
        }
    }


    private final static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger();

    /**
     * 锁定并拿走奖品
     *
     * @param actId    活动id
     * @param trophyId 奖品ID
     * @return 是否成功获取奖品
     */
    public boolean robAward(Long actId, Long groupId, Long trophyId) {

        requireNonNull(actId);
        requireNonNull(groupId);
        requireNonNull(trophyId);
        boolean result = decrTrophyLeftCount(actId, groupId, trophyId);
//        if (result != false) {
//            logger.info("抢{}成功,groupId:{},result={}", trophyId, groupId, result);
//        }
        return result;
    }


    /**
     * 将失败的奖品回滚放回奖池
     *
     * @param actId    活动id
     * @param trophyId 奖品ID
     * @return 是否成功获取奖品
     */
    public void putBackAward(Long actId, Long groupId, Long trophyId) {

        requireNonNull(actId);
        requireNonNull(trophyId);
        incrTrophyLeftCount(actId, groupId, trophyId);
    }

    public void saveValidateOpenId(String openId) {
        requireNonNull(openId);
        String key = MessageFormat.format(validateOpenId, openId);
//        redisOperateUtil.set(key,"1");
        redisOperateUtil.setValueAndLifeCycle(key, 5 * 60, "1");
    }

    /**
     * 判断openId是否合法
     *
     * @param openId openId
     * @return true 合法  false 非法
     */
    public boolean isValidateOpenId(String openId) {
        requireNonNull(openId);
        String key = MessageFormat.format(validateOpenId, openId);
        if (redisOperateUtil.keyExists(key)) {
            return true;
        } else {
            return false;
        }
    }

//    public ActRuleInfo getActRuleFromCache(Long actId) {
//        requireNonNull(actId);
//
//        String key = MessageFormat.format(actRuleInfo, actId);
//
//        String value = redisOperateUtil.get(key);
//        ActRuleInfo actRuleInfo;
//        if (value == null) {
//            ActParameterInfo dayPartTime = actParameterQueryService.queryActParameterBy(actId, ParameterType.PART, ParameterKey.PART_DAY_TIMES);
//            ActParameterInfo totalPartTime = actParameterQueryService.queryActParameterBy(actId, ParameterType.PART, ParameterKey.PART_TOTAL_TIMES);
//            ActParameterInfo dayLotteryTime = actParameterQueryService.queryActParameterBy(actId, ParameterType.LOTTERY, ParameterKey.LOTTERY_DAY_TIMES);
//            ActParameterInfo totalLotteryTime = actParameterQueryService.queryActParameterBy(actId, ParameterType.LOTTERY, ParameterKey.LOTTERY_TOTAL_TIMES);
//            actRuleInfo = new ActRuleInfo();
//            actRuleInfo.setPartDayTimes(dayPartTime.numberValue == null ? BigDecimal.ZERO : dayPartTime.numberValue);
//            actRuleInfo.setPartTotalTimes(totalPartTime.numberValue == null ? BigDecimal.ZERO : totalPartTime.numberValue);
//            actRuleInfo.setLotteryDayTimes(dayLotteryTime.numberValue == null ? BigDecimal.ZERO : dayLotteryTime.numberValue);
//            actRuleInfo.setLotteryTotalTimes(totalLotteryTime.numberValue == null ? BigDecimal.ZERO : totalLotteryTime.numberValue);
//            redisOperateUtil.set(key, JSON.toJSONString(actRuleInfo));
//            return actRuleInfo;
//        } else {
//            actRuleInfo = JSON.parseObject(value, ActRuleInfo.class);
//        }
//        return actRuleInfo;
//
//    }



    /**
     * 清楚用户数据缓存
     *
     * @param actId
     */
    public void cleanPlayerDataCache(Long actId) {

        String formatActId = MessageFormat.format("{0}", actId);
        try {
            String command = "/usr/local/bin/redis-cli -n 0 keys actId_" + formatActId + "* |xargs /usr/local/bin/redis-cli -n 0 del";
            String result = exec(command);
            logger.info("执行linux命令:{} 返回结果:{}", command, result);
        } catch (Exception e) {
            logger.error("执行redis批量删除命令失败", e);
        }

    }


    public static String exec(String cmd) {
        logger.info("开始执行linux命令:{}", cmd);
        Runtime runtime = Runtime.getRuntime();
        try {
            String[] cmdA = {"/bin/sh", "-c", cmd};
            Process process = runtime.exec(cmdA);
            int code = process.waitFor();
            LineNumberReader br = new LineNumberReader(new InputStreamReader(
                    process.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            logger.info("执行完毕，退出代码：{}，返回结果:{}", code, line);
            return sb.toString();
        } catch (Exception e) {
            logger.error("执行linux命令出错", e);
        }
        return "异常退出";
    }



    //获取圣火坛排名
    public Long getTorchRank(Long actId) {
        requireNonNull(actId);
        String key = MessageFormat.format(torchRankKey, actId);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            return redisOperateUtil.incr(key);
        } else {
            return redisOperateUtil.incr(key);
        }
    }


    //获取玩家数
    public long getPlayers(Long actId) {
        requireNonNull(actId);
        String key = MessageFormat.format(playerKey, actId);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            return redisOperateUtil.incr(key);
        } else {
            return Long.parseLong(value);
        }
    }

    public long incrPlayers(Long actId) {
        requireNonNull(actId);
        String key = MessageFormat.format(playerKey, actId);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            return redisOperateUtil.incr(key);
        } else {
            return redisOperateUtil.incr(key);
        }
    }

//    public GamesTimeInfo getGameTimeInfo(Long actId, CredentialType type, String credentialValue) {
//        int dayPartTimes = getUserTodayPlayTimes(actId, credentialValue, type);
//        int totalPartTimes = getUserTotalPlayTimes(actId, credentialValue, type);
//
//        int dayLotteryTimes = getUserTodayLotteryTimes(actId, credentialValue, type);
//        int totalLotteryTimes = getUserTotalLotteryTimes(actId, credentialValue, type);
//
//        return new GamesTimeInfo(dayPartTimes, totalPartTimes, dayLotteryTimes, totalLotteryTimes);
//    }


    /**
     * 火炬传播人数加1
     *
     * @param actId  活动共D
     * @param nodeId nodeId
     */
    public void incrFollowers(Long actId, Long nodeId) {
        String key = MessageFormat.format(subChildren, actId, nodeId);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            redisOperateUtil.set(key, "1");
        } else {
            redisOperateUtil.incr(key);
        }
    }

    /**
     * 获取火炬传播人数
     *
     * @param actId  活动ID
     * @param nodeId nodeId
     * @return
     */
    public Long getFolllowers(Long actId, Long nodeId) {
        String key = MessageFormat.format(subChildren, actId, nodeId);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            return 0L;
        } else {
            return Long.parseLong(value);
        }
    }

    /**
     * 获取上次定时任务最后更新的nodeId
     *
     * @return nodeId
     */
    public Long getLastUpdateNodeId() {
        String value = redisOperateUtil.get(lastUpdateNodeId);
        if (value == null) {
            return 0L;
        } else {
            return Long.parseLong(value);
        }
    }

    /**
     * 设置定时任务最后更新的nodeId
     *
     * @param nodeId nodeId
     */
    public void setLastUpdateNodeId(Long nodeId) {
        redisOperateUtil.set(lastUpdateNodeId, nodeId + "");
    }

    /**
     * 获取活动列表缓存数据
     *
     * @param actId 活动ID
     */
    public Optional<String> getActDisplayList(Long actId) {
        requireNonNull(actId);
        String key = MessageFormat.format(actDisplayList, actId);
        String value = redisOperateUtil.get(key);
        return Optional.ofNullable(value);
    }


    /**
     * 获取活动列表缓存数据
     *
     * @param actId 活动ID
     */
    public void setActDisplayList(Long actId, String jsonValue) {
        requireNonNull(actId);
        String key = MessageFormat.format(actDisplayList, actId);
        //缓存60秒
        redisOperateUtil.setValueAndLifeCycle(key, 60, jsonValue);
    }

    /**
     * 获取用户分享次数
     */
    public int getShareTimes(Long actId, CredentialType type, String credential) {
        requireNonNull(actId);
        requireNonNull(type);
        requireNonNull(credential);

        String key = MessageFormat.format(shareTimes, actId, type, credential);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            return 0;
        } else {
            return Integer.parseInt(value);
        }

    }

    /**
     * 用户分享次数+1
     */
    public void addShareTimes(Long actId, CredentialType type, String credential) {
        requireNonNull(actId);
        requireNonNull(type);
        requireNonNull(credential);
        String key = MessageFormat.format(shareTimes, actId, type, credential);
        String value = redisOperateUtil.get(key);
        if (value == null) {
            redisOperateUtil.set(key, "1");
        } else {
            redisOperateUtil.incr(key);
        }
    }

    /**
     * 获取流量套餐
     */
//    public JSONObject getPhoneFlowPlan() {
//        String value = redisOperateUtil.get(phoneFlowPlan);
//        if (value == null) {
//            JSONObject result = MobileFlowUtil.getAllPlan();
//            if (result.getString("error_code").equals("0")) {
//                redisOperateUtil.setValueAndLifeCycle(phoneFlowPlan, 24 * 60 * 60, JSONObject.toJSONString(result, SerializerFeature.DisableCircularReferenceDetect));
//            }
//            return result;
//        } else {
//            return JSON.parseObject(value);
//        }
//    }


    /**
     * 将验证码暂存redis
     *
     * @param type           凭证类型
     * @param authKey        凭证
     * @param verifyCodeType 验证码类型
     * @param verifyCode     验证码
     * @param expireSeconds  有效秒数
     */
//    public void setVerifyCode(CredentialType type,
//                              String authKey,
//                              VerifyCodeType verifyCodeType,
//                              String verifyCode,
//                              int expireSeconds) {
//        requireNonNull(type);
//        requireNonNull(authKey);
//        requireNonNull(verifyCode);
//        requireNonNull(verifyCodeType);
//        String key = MessageFormat.format(verifyCodeKey, type, authKey, verifyCodeType);
//        if (verifyCodeType.equals(VerifyCodeType.SMS) && redisOperateUtil.keyExists(key + "_timeout")) {
//            throw new IllegalStateException("发送太频繁，请稍后再试");
//        }
//        if (verifyCodeType.equals(VerifyCodeType.SMS)) {
//            redisOperateUtil.setValueAndLifeCycle(key + "_timeout", smsVerifyCodeTimeInterval, "1");
//        }
//        redisOperateUtil.setValueAndLifeCycle(key, expireSeconds, verifyCode);
//    }

    /**
     * 验证验证码是否正确
     * 如果正确，就清除缓存
     *
     * @param type           凭证类型
     * @param authKey        凭证
     * @param verifyCodeType 验证码类型
     * @param verifyCode     验证码
     * @param isDelete       是否验证完毕后删除验证码
     */
//    public boolean checkVerifyCode(CredentialType type, String authKey, VerifyCodeType verifyCodeType, String verifyCode, boolean isDelete) {
//        requireNonNull(type);
//        requireNonNull(authKey);
//        requireNonNull(verifyCode);
//        String key = MessageFormat.format(verifyCodeKey, type, authKey, verifyCodeType);
//        String value = redisOperateUtil.get(key);
//        if (!Strings.isNullOrEmpty(value) && value.equals(verifyCode)) {
//            if (isDelete) {
//                redisOperateUtil.deleteKey(key);
//            }
//            return true;
//        }
//        return false;
//    }

    /**
     * 记录IP抽奖次数
     */
    public void countIPTimes(Long actId, String ipAddress) {
        requireNonNull(actId);
        requireNonNull(ipAddress);

        String key = MessageFormat.format(ipCounts, actId, ipAddress);
        String value = redisOperateUtil.get(key);
        if (Strings.isNullOrEmpty(value)) {
            redisOperateUtil.set(key, "1");
        } else {
            redisOperateUtil.incr(key);
        }
    }

    /**
     * 获取IP抽奖次数
     */
    public int getIPTimes(Long actId, String ipAddress) {
        requireNonNull(actId);
        requireNonNull(ipAddress);

        String key = MessageFormat.format(ipCounts, actId, ipAddress);
        String value = redisOperateUtil.get(key);
        if (Strings.isNullOrEmpty(value)) {
            return 0;
        } else {
            return Integer.parseInt(value);
        }
    }

    /**
     * 获取活动接口请求IP次数上限
     */
    public int getActIPLimit(Long actId) {
        requireNonNull(actId);

        String key = MessageFormat.format(ipReqTimesLimit, actId);
        String value = redisOperateUtil.get(key);
        if (value != null) {
            return Integer.parseInt(value);
        } else {
            return Integer.MAX_VALUE;
        }

    }


//    public long incrOpenIdPv(Long actId) {
//        requireNonNull(actId);
//        String key = MessageFormat.format(openIdPvKey, actId);
//        if (redisOperateUtil.get(key) == null) {
//            redisOperateUtil.set(key, "1");
//            return 1l;
//        } else {
//            return redisOperateUtil.incr(key);
//        }
//    }

    /**
     * 批量入队操作
     *
     * @param listName 队列名称
     * @param values   值
     */
    public void rpush(String listName, String... values) {

        requireNonNull(listName);
        requireNonNull(values);

        redisOperateUtil.rpush(listName, values);
    }

    /**
     * 原子性操作出队并且将出队的值放到备份队列（安全队列）
     *
     * @param listName       队列名称
     * @param backUpListName 备份队列名称
     * @param popNum         要出队的个数
     * @return 出队的元素列表
     */
    public List<String> brpoplpush(String listName, String backUpListName, Integer popNum) {

        requireNonNull(listName);
        requireNonNull(backUpListName);
        requireNonNull(popNum);

        return redisOperateUtil.brpoplpush(listName, backUpListName, popNum);

    }


    /**
     * 标记该手机号已中奖
     *
     * @param actId 活动ID
     * @param phone 手机号
     */
    public void markPhoneLottery(Long actId, String phone) {
        requireNonNull(actId);
        requireNonNull(phone);
        String key = MessageFormat.format(markPhoneLotteryKey, actId, phone);
        redisOperateUtil.set(key, "1");
    }


    /**
     * 判断手机号是否已经中奖奖
     *
     * @return true 已中过  false 没中
     */
    public boolean isLotteryBefore(Long actId, String phone) {
        requireNonNull(actId);
        requireNonNull(phone);
        String key = MessageFormat.format(markPhoneLotteryKey, actId, phone);
        if (redisOperateUtil.keyExists(key)) {
            return true;
        }
        return false;
    }


    private final String wechatOpenPlatformPostData = "wechat:openPlatformXmlData";

    /**
     * 保存微信开放平台每隔10分钟推送一次的用户获取ticket的xml数据
     *
     * @param ticket
     */
    public void saveWechatOpenPlatFormData(String ticket) {
        if (!Strings.isNullOrEmpty(ticket)) {
            redisOperateUtil.set(wechatOpenPlatformPostData, ticket);
        } else {
            throw new IllegalArgumentException("ticket为空");
        }
    }

    /**
     * 获取微信开放平台每隔10分钟推送一次的用户获取ticket的xml数据
     */
    public String getWechatOpenPlatFormData() {
        String data = redisOperateUtil.get(wechatOpenPlatformPostData);
        if (!Strings.isNullOrEmpty(data)) {
            return data;
        } else {
            throw new IllegalStateException("redis中查无相关的xml数据");
        }
    }

    private final String groupIdKey = "actId_{0}:actGroupIds";

    public List<Long> getGroupIdsByActId(Long actId) {
        requireNonNull(actId);
        String key = MessageFormat.format(groupIdKey, actId);
        String value = redisOperateUtil.get(key);
        if (value != null) {
            return JSON.parseArray(value, Long.class);
        } else {
            //TODO
            List<Long> groupIds = null;
//                    trophyQueryService.queryTrophy(actId, 1, 0)
//                    .content
//                    .stream()
//                    .map(actTrophyInfo -> actTrophyInfo.groupId)
//                    .collect(Collectors.toList());

            redisOperateUtil.setValueAndLifeCycle(key, 24 * 60 * 60, JSON.toJSONString(groupIds, SerializerFeature.DisableCircularReferenceDetect));
            return groupIds;
        }
    }

    public void cleanActGroupIds(Long actId) {
        requireNonNull(actId);
        String key = MessageFormat.format(groupIdKey, actId);
        redisOperateUtil.deleteKey(key);
    }

    /**
     * 通过分享获得增益的次数+1
     *
     * @param actId         获得ID
     * @param shareBuffType 分享增益类型
     * @param type          凭证类型
     * @param credential    凭证
     * @return +1后的次数
     */
    public Long incrShareCaseTimes(Long actId,
                                   ShareBuffType shareBuffType,
                                   Long trophyId,
                                   CredentialType type,
                                   String credential) {
        requireNonNull(actId);
        requireNonNull(shareBuffType);
        requireNonNull(trophyId);
        requireNonNull(type);
        requireNonNull(credential);

        String key = MessageFormat.format(addShareCaseTimes,
                actId,
                shareBuffType,
                trophyId,
                type,
                credential);
        return redisOperateUtil.incr(key);
    }


    @Autowired
    ITrophyItemQueryService trophyItemQueryService;

    private final String drawInfoCacheKey = "actId_{0}:drawInfoCache:{1}_{2}";

    private final String drawInfoCacheSearchKey = "actId_{0}:drawInfoCache:*";

    /**
     * 查询奖品的缓存维护
     *
     * @param actId      活动ID
     * @param type       凭证类型
     * @param credential 凭证
     * @return
     */
    public List<DrawInfo> getDrawInfosFromCacheOrDatabase(Long actId,
                                                          CredentialType type,
                                                          String credential) {
        requireNonNull(actId);
        requireNonNull(type);
        requireNonNull(credential);

        String key = MessageFormat.format(drawInfoCacheKey,
                actId,
                type,
                credential);
        String value = redisOperateUtil.get(key);

        if (Strings.isNullOrEmpty(value)) {
            List<DrawInfo> infos = trophyItemQueryService.queryItemsByCredential(actId,
                    type,
                    credential);
            redisOperateUtil.setValueAndLifeCycle(key, 60 * 60, JSONArray.toJSONString(infos, SerializerFeature.DisableCircularReferenceDetect));

            return infos;
        } else {
            return JSONArray.parseArray(value, DrawInfo.class);
        }
    }

    public void addDrawInfosToCache(Long actId,
                                    CredentialType type,
                                    String credential,
                                    DrawInfo info) {
        requireNonNull(actId);
        requireNonNull(type);
        requireNonNull(credential);
        requireNonNull(info);
        String key = MessageFormat.format(drawInfoCacheKey,
                actId,
                type,
                credential);
        String value = redisOperateUtil.get(key);
        List<DrawInfo> infos = new ArrayList<>();
        if (Strings.isNullOrEmpty(value)) {
            infos.add(info);
        } else {
            infos = JSONArray.parseArray(value, DrawInfo.class);
            List<DrawInfo> newList = new ArrayList<>();
            newList.add(info);
            newList.addAll(infos);
        }
        redisOperateUtil.setValueAndLifeCycle(key, 60 * 60, JSONArray.toJSONString(infos, SerializerFeature.DisableCircularReferenceDetect));
    }

    public void deletePersonDrawInfoCache(Long actId,
                                          CredentialType type,
                                          String credential) {
        requireNonNull(actId);
        requireNonNull(type);
        requireNonNull(credential);

        String key = MessageFormat.format(drawInfoCacheKey,
                actId,
                type,
                credential);
        redisOperateUtil.deleteKey(key);
    }

    private final static String GROUP_CACHE_KEY = "actId_{0}:groupsCache";

    public List<ActGroupInfo> getGroupsCache(Long actId) {
        String key = MessageFormat.format(GROUP_CACHE_KEY,
                actId);
        String value = redisOperateUtil.get(key);
        if (Strings.isNullOrEmpty(value)) {
            List<ActGroupInfo> groupInfos = actGroupQueryService.getGroups(actId);
            redisOperateUtil.setValueAndLifeCycle(key, 60 * 60, JSON.toJSONString(groupInfos, SerializerFeature.DisableCircularReferenceDetect));
            return groupInfos;
        } else {
            return JSON.parseArray(value, ActGroupInfo.class);
        }
    }


    private final static String SHARE_CASE_CACHE_KEY = "actId_{0}:shareCaseCache";

    @Autowired
    IShareCaseQueryService shareCaseQueryService;

    public List<ShareCaseInfo> getShareCaseCache(Long actId) {
        String key = MessageFormat.format(SHARE_CASE_CACHE_KEY,
                actId);
        String value = redisOperateUtil.get(key);
        if (Strings.isNullOrEmpty(value)) {
            List<ShareCaseInfo> shareCaseInfos = shareCaseQueryService.queryShareCaseByActId(actId);
            redisOperateUtil.setValueAndLifeCycle(key, 60 * 2, JSON.toJSONString(shareCaseInfos, SerializerFeature.DisableCircularReferenceDetect));
            return shareCaseInfos;
        } else {
            return JSON.parseArray(value, ShareCaseInfo.class);
        }
    }


    private final static String CUSTOMER_INFO_CACHE_KEY = "actId_{0}:customerInfo:{1}_{2}";

    @Autowired
    ICustomerQueryService customerQueryService;


    public CustomerInfo getCustomerInfo(String companyId,Long actId, CredentialType type, String credential) {
        String key = MessageFormat.format(CUSTOMER_INFO_CACHE_KEY, actId, type, credential);
        String value = redisOperateUtil.get(key);
        if (Strings.isNullOrEmpty(value)) {
            CustomerInfo customerInfo = customerQueryService.getInfoByCredential(companyId,type, credential);
            redisOperateUtil.setValueAndLifeCycle(key, 60 * 30, JSON.toJSONString(customerInfo, SerializerFeature.DisableCircularReferenceDetect));
            return customerInfo;
        } else {
            return JSONObject.parseObject(value, CustomerInfo.class);
        }
    }


    private final static String ATTACH_LABEL_CACHE_KEY = "actId_{0}:customerId:{1}";

    @Autowired
    ILabelQueryService labelQueryService;


    public List<LabelInfo> getAttachLabelsCache(Long actId, String customerId) {
        String key = MessageFormat.format(ATTACH_LABEL_CACHE_KEY, actId, customerId);
        String value = redisOperateUtil.get(key);
        if (Strings.isNullOrEmpty(value)) {
//            List<LabelInfo> labelInfos = labelQueryService.getAttachedLabels(customerId, 1, 0).content;
//            redisOperateUtil.setValueAndLifeCycle(key, 60 * 30, JSON.toJSONString(labelInfos, SerializerFeature.DisableCircularReferenceDetect));
//            return labelInfos;
            return  null;
        } else {
            return JSON.parseArray(value, LabelInfo.class);
        }
    }


    private final static String ACT_GROUP_LABELS_CACHE_KEY = "actId_{0}:groupId_{1}";

    public boolean isContainsLabel(Long actId, Long groupId, List<Long> labelIds) {
        logger.entry(actId, groupId, labelIds);
        Objects.requireNonNull(actId);
        Objects.requireNonNull(groupId);
        Objects.requireNonNull(labelIds);

        String key = MessageFormat.format(ATTACH_LABEL_CACHE_KEY, actId, groupId);
        String value = redisOperateUtil.get(key);
        List<Long> groupLabelIds;
        if (Strings.isNullOrEmpty(value)) {
            groupLabelIds = actGroupQueryService.getGroupLabelIds(groupId);
            redisOperateUtil.setValueAndLifeCycle(key, 60 * 30, JSON.toJSONString(groupLabelIds, SerializerFeature.DisableCircularReferenceDetect));
        } else {
            groupLabelIds = JSON.parseArray(value, Long.class);
        }

        return groupLabelIds.stream().anyMatch(id -> labelIds.contains(id));
    }




}
