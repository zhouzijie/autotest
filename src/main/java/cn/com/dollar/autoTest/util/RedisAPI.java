package cn.com.dollar.autoTest.util;//package util;
//
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//import redis.clients.jedis.ShardedJedisPool;
//
//public class RedisAPI {
//    private static JedisPool pool = null;
//    private static JedisPool masterPool = null;
//    private static JedisPool slavePool = null;
//    private static ShardedJedisPool shardedJedisPool;//切片连接池
//    private static int workingPool=0;   //正在使用的连接池 0:没有   1:主服务器   2：从服务器
//
//    /**
//     * 构建redis连接池
//     *
//
//     * @return JedisPool
//     */
//    public static JedisPool getPool() {
//        if (pool == null) {
//            JedisPoolConfig config = new JedisPoolConfig();
//            //控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；
//            //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
//            config.setMaxActive(10000);
//            //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
//            config.setMaxIdle(500);
//            //表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
//            config.setMaxWait(1000);
//            //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
//            config.setTestOnBorrow(true);
//            pool = new JedisPool(config, "127.0.0.1", 6379);
//        }
//        return pool;
//    }
//
//    public static JedisPool getMasterPool() {
//        if (masterPool == null) {
//            JedisPoolConfig config = new JedisPoolConfig();
//            //控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；
//            //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
//            config.setMaxActive(10000);
//            //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
//            config.setMaxIdle(8000);
//            //表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
//            config.setMaxWait(1000);
//            //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
//            config.setTestOnBorrow(true);
//            if(Configs.masterPassword.equals("")){
//            	masterPool = new JedisPool(config, Configs.masterPoolIp, Configs.masterPoolPort,2000);
//            }else{
//            	masterPool = new JedisPool(config, Configs.masterPoolIp, Configs.masterPoolPort,2000,Configs.masterPassword);
//            }
//        }
//        return masterPool;
//    }
//
//    public static JedisPool getSlavePool() {
//        if (slavePool == null) {
//            JedisPoolConfig config = new JedisPoolConfig();
//            //控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；
//            //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
//            config.setMaxActive(10000);
//            //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
//            config.setMaxIdle(8000);
//            //表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
//            config.setMaxWait(1000);
//            //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
//            config.setTestOnBorrow(true);
//            if(Configs.slavePassword.equals("")){
//            	slavePool = new JedisPool(config, Configs.slavePoolIp, Configs.slavePoolPort,1000);
//            }else{
//            	slavePool = new JedisPool(config, Configs.slavePoolIp, Configs.slavePoolPort,1000,Configs.slavePassword);
//            }
//        }
//        return slavePool;
//    }
//
//    /**
//     * 返还到连接池
//     *
//     * @param pool
//     * @param redis
//     */
//
//    public static void returnResource(JedisPool pool, Jedis redis) {
//        if (redis != null) {
//            pool.returnResource(redis);
//        }
//    }
//
//    public static void returnMyResource(Jedis redis) {
//        getMasterPool().returnResource(redis);
//    }
//
//
//    public static Jedis getMyResource(JedisPool masterPool,JedisPool slavePool){
//    	Jedis master=null;
//    	master=masterPool.getResource();
//    	return master;
//    }
//
//    /**
//     * 获取数据
//     *
//     * @param key
//     * @return
//     */
//    public static String get(String key){
//        String value = null;
//
//        JedisPool pool = null;
//        Jedis jedis = null;
//        try {
//            pool = getPool();
//            jedis = pool.getResource();
//            value = jedis.get(key);
//        } catch (Exception e) {
//            //释放redis对象
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            //返还到连接池
//            returnResource(pool, jedis);
//        }
//
//        return value;
//    }
//
//}