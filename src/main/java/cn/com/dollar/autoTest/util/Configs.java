package cn.com.dollar.autoTest.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Configs {
//	测试平台配置好的活动ID
	public static final Long ipTestActId=2L;
	public static final Long userVertifyActId=3L;
	public static final Long actRulesActId=4L;
	public static final Long actTimeLimitActId=5L;
	public static final Long shareCaseActId=6L;
	public static final Long shareCaseModelActId=7L;
	public static final Long userGroupActId=8L;
	public static final Long pageViewActId=67L;

	public static final String getPageViewAlias="DLc7fcac4e600844959c33275f99f9b5ba";

	//用于清数据的
	public static List<Long> actIds=new ArrayList<>();

	public static final String companyId="34eb35f02786481580e4541db1e9054e";
	//用户分组模板文件路径
	public static String userFileAddress;
	//每个人的埋点事件的总在线时长
	public static final Long totalTime=30L;

	public static String engineBaseURL;
	public static String portalBaseURL;
	public static String authzBaseURL;
	public static String drawURL;
	public static String shareURL;
	public static String passURL;
	public static String createMainURL;
	public static String adminCreateMainURL;
	public static String actParameterURL;
	public static String loginURL;
	public static String cleanTestDataURL;
	public static String trophyURL;
	public static String groupURL;
	public static String groupRuleURL;
	public static String shareCaseURL;
	public static String channelURL;
	public static String labelURL;
	public static String uploadLabelUsersURL;
	public static String importLabelsURL;
	public static String pageViewURL;
	public static String queryActivityBaseInfoURL;
	public static String queryUserBehaviorTotalDataRUL;
	public static String queryActivityTotalStaURL;
	public static String queryUserBehaviorDataURL;
	public static String shareDegreeURL;
	public static String shareDegreeTopTenRUL;
	public static String distributionURL;

	static {
		InputStream config = Configs.class.getClassLoader()
				.getResourceAsStream("autoTest.properties");
		InputStream in = new BufferedInputStream(config);
		Properties props = new Properties();
		try {
			props.load(in);

// ---------------------------------接口地址-------------------------------------------------------
			engineBaseURL=props.getProperty("baseUrl.engine");
			portalBaseURL=props.getProperty("baseUrl.portal");
			authzBaseURL=props.getProperty("baseUrl.authz");

			drawURL=props.getProperty("engine.draw");
			shareURL=props.getProperty("engine.share");
			passURL=props.getProperty("engine.pass");
			pageViewURL=props.getProperty("engine.statistics");

			createMainURL=props.getProperty("portal.createMain");
			adminCreateMainURL=props.getProperty("portal.adminCreateMain");
			actParameterURL=props.getProperty("portal.actParameter");
			cleanTestDataURL=props.getProperty("portal.cleanTestData");
			trophyURL=props.getProperty("portal.trophy");
			groupURL=props.getProperty("portal.group");
			groupRuleURL=props.getProperty("portal.groupRule");
			shareCaseURL=props.getProperty("portal.shareCase");
			channelURL=props.getProperty("portal.channel");
			labelURL=props.getProperty("portal.label");
			uploadLabelUsersURL=props.getProperty("portal.uploadLabelUsers");
			importLabelsURL=props.getProperty("portal.importLabelsURL");
			queryActivityBaseInfoURL=props.getProperty("portal.queryActivityBaseInfo");
			queryUserBehaviorTotalDataRUL=props.getProperty("portal.queryUserBehaviorTotalData");
			queryActivityTotalStaURL=props.getProperty("portal.queryActivityTotalSta");
			queryUserBehaviorDataURL=props.getProperty("portal.queryUserBehaviorData");
			shareDegreeURL=props.getProperty("portal.shareDegree");
			shareDegreeTopTenRUL=props.getProperty("portal.shareDegreeTopTen");
			distributionURL=props.getProperty("portal.distribution");

			loginURL=props.getProperty("authz.login");
//--------------------------------------------------------------------------------------------------------------------
			userFileAddress=props.getProperty("userFileAddress");
//--------------------------------------------------------------------------------------------------------------------
			actIds.add(ipTestActId);
			actIds.add(userVertifyActId);
			actIds.add(actRulesActId);
			actIds.add(actTimeLimitActId);
			actIds.add(shareCaseActId);
			actIds.add(shareCaseModelActId);
			actIds.add(userGroupActId);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				in.close();
				config.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(Configs.drawURL);
	}
}
