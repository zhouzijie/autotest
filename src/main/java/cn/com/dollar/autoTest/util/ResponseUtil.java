package cn.com.dollar.autoTest.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZZJ on 2016-11-16.
 */
public class ResponseUtil {
    public Map<String,Object> sendResult(List list){
        Map<String,Object> map=new HashMap<String, Object>();
        if(list.size()==0){
            map.put("status",true);
        }
        else{
            map.put("status",false);
        }
        map.put("result",list);
        return map;
    }

    public Map<String,Object> sendResult(boolean status,String msg){
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("status",status);
        map.put("result",msg);
        return map;
    }

    public Map<String,Object> sendResult(boolean status,JSONArray msg){
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("status",status);
        map.put("result",msg);
        return map;
    }

}
