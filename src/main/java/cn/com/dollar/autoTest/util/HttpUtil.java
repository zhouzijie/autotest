package cn.com.dollar.autoTest.util;

import cn.com.dollar.autoTest.service.implement.ActMainServiceImp;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by ZZJ on 2017-2-15.
 */
public class HttpUtil {

    public static JSONObject post(String url,Object o,String token){
        HttpPost post=new HttpPost(url);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = null;

        int stateCode=500;
        RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(10000)
                .setConnectTimeout(10000).setSocketTimeout(10000).build();
        try {
            post.setConfig(config);
            //这里的判断是为了兼容活动平台的登录接口
            if(o!=null){
                post.addHeader("Content-type","application/json; charset=utf-8");
                post.setHeader("Accept", "application/json");
                post.setEntity(new StringEntity(JSONObject.toJSON(o).toString(), "UTF-8"));
            }else{
                post.addHeader("Content-type","application/x-www-form-urlencoded; charset=utf-8");
            }
            if(token!=null&&token!=""){
                post.setHeader("token", token);
            }
            response = httpClient.execute(post);
            stateCode = response.getStatusLine().getStatusCode();
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {
                result = convertStreamToString(resEntity.getContent(), "UTF-8");
            }
            EntityUtils.consume(resEntity);

        }  catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(response!=null){
                    response.close();
                }
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return getRes(stateCode,result);
    }

    public static JSONObject get(String url,Map<String,Object> paramMap){
        if(paramMap!=null){
            String param = "?";
            for(String key: paramMap.keySet()){
                try {
                    param +=key+"="+URLEncoder.encode(paramMap.get(key).toString(),"utf-8")+"&";
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
            url =url + param.substring(0, param.length()-1);
        }

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = null;
        int stateCode=500;
        try{
            HttpGet httpGet = new HttpGet(url);
            response = httpClient.execute(httpGet);
            stateCode = response.getStatusLine().getStatusCode();
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                result = convertStreamToString(resEntity.getContent(), "UTF-8");
            }
            EntityUtils.consume(resEntity);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return getRes(stateCode,result);
    }

    public static JSONObject put(String url,String token,Object o){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = null;
        int stateCode=500;

        try{
            HttpPut httpPut = new HttpPut(url);
            httpPut.addHeader("Content-type","application/json; charset=utf-8");
            httpPut.setHeader("Accept", "application/json");
            if(token!=null){
                httpPut.setHeader("token", token);
            }
            if(o!=null){
                httpPut.setEntity(new StringEntity(JSONObject.toJSON(o).toString(), "UTF-8"));
            }
            response = httpClient.execute(httpPut);
            stateCode = response.getStatusLine().getStatusCode();
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                result = convertStreamToString(resEntity.getContent(), "UTF-8");
            }
            EntityUtils.consume(resEntity);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return getRes(stateCode,result);


    }

    public static JSONObject delete(String url,String token){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = null;
        int stateCode=500;

        try{
            HttpDelete httpDelete = new HttpDelete(url);
            if(token!=null){
                httpDelete.addHeader("token", token);
            }
            response = httpClient.execute(httpDelete);
            stateCode = response.getStatusLine().getStatusCode();
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                result = convertStreamToString(resEntity.getContent(), "UTF-8");
            }
            EntityUtils.consume(resEntity);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return getRes(stateCode,result);


    }

    public static JSONObject postFile(String url,Map<String,Object> paramMap,File file,String fileParaName,String token){
        int stateCode=500;
        String result = null;

        HttpPost post=new HttpPost(url);
        post.setHeader("token", token);

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        try {
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            FileBody fileBody = new FileBody(file);
            builder.addPart(fileParaName, fileBody);

            for(String key: paramMap.keySet()) {
                StringBody stringBody = new StringBody(paramMap.get(key).toString(), ContentType.MULTIPART_FORM_DATA);
                builder.addPart(key, stringBody);
            }

            HttpEntity entity = builder.build();
            post.setEntity(entity);

            response = httpClient.execute(post);
            stateCode = response.getStatusLine().getStatusCode();
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {
                result = convertStreamToString(resEntity.getContent(), "UTF-8");
            }
            EntityUtils.consume(resEntity);

        }catch ( Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(response!=null){
                    response.close();
                }
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return getRes(stateCode,result);
    }
    public static String convertStreamToString(InputStream is, String charset)
            throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(is,
                charset)) {
            try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line).append("\r\n");
                }
            }
        }
        return stringBuilder.toString();
    }

    private static JSONObject getRes(Integer stateCode,String result){
        JSONObject res=new JSONObject();
        if(stateCode>=200&&stateCode<300){
            if(result!=null&&!result.equals("")){
                try{
                    res=JSONObject.parseObject(result);
                }catch (ClassCastException e){
                    res.put("dataArray", JSONObject.parseArray(result));
                }
            }
            res.put("code",stateCode);
            return res;
        }
        else{
            res.put("code",stateCode);
            try{
                res.put("msg",JSONObject.parseObject(result));
            }catch (ClassCastException e){
                res.put("msg", JSONObject.parseArray(result));
            }
            return res;
        }
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
//        JSONObject res= ActMainServiceImp.getLoginMessage();
//        String token=res.getString("access_token");
//        res=HttpUtil.delete(Configs.portalBaseURL+Configs.cleanTestDataURL+"/"+Configs.ipTestActId,token);
//        System.out.println(res.toString());

        String result="[169]";
        System.out.println(JSONArray.parse(result));

    }
}
