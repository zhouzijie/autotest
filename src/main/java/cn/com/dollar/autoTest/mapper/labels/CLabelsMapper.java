package cn.com.dollar.autoTest.mapper.labels;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by ZZJ on 2017-4-28.
 */
public interface CLabelsMapper {
    List<Long> getLabelIdsByName(@Param(value = "tableName")String tableName, @Param(value = "labelName") String labelName );
    int deleteLabelsByIds(@Param(value = "tableName") String tableName,@Param(value = "ids") List<Long> ids);
}
