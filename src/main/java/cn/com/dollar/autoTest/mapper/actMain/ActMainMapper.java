package cn.com.dollar.autoTest.mapper.actMain;

import cn.com.dollar.engine.act.main.model.ActivityMainInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by ZZJ on 2016-11-15.
 */
public interface ActMainMapper {
    public Date getEndDate(BigDecimal actId);

    public int updateAct(ActivityMainInfo activityMainInfo);

    public List<ActivityMainInfo> getActByName(String name);

    public int deleteActByIds(@Param(value = "ids") List<Long> ids);
}
