package cn.com.dollar.autoTest.mapper.shareCase;

import cn.com.dollar.autoTest.model.ShareCase;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by ZZJ on 2016-11-24.
 */
public interface ShareCaseMapper {
    public List<ShareCase> getShareCaseInfo(BigDecimal actId);
}
