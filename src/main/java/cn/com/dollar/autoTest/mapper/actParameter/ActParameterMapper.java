package cn.com.dollar.autoTest.mapper.actParameter;


import cn.com.dollar.engine.act.parameter.model.type.ParameterKey;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-11-15.
 */
public interface ActParameterMapper {
    public int getActParameter(@Param(value = "actId") BigDecimal actId, @Param(value = "parameterKey") ParameterKey parameterKey);
}
