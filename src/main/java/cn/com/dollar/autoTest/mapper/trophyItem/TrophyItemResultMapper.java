package cn.com.dollar.autoTest.mapper.trophyItem;

import cn.com.dollar.engine.trophy.item.model.TrophyItem;
import cn.com.dollar.engine.trophy.item.model.TrophyItemInfo;
import cn.com.dollar.autoTest.model.TrophyItemCount;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by ZZJ on 2016-11-11.
 */
public interface TrophyItemResultMapper {
    public List<Integer> drawTimesCheck(@Param(value = "actId") BigDecimal actId, @Param(value = "dayDrawTimes") int dayDrawTimes);
    public  List<Integer>  lotteryTimesCheck(@Param(value = "actId") BigDecimal actId, @Param(value = "dayLotteryTimes") int dayLotteryTimes);
    public  List<String> callerIpCheck(BigDecimal actId);
    public  List<Integer>  credentialWithPhones(BigDecimal actId);
    public  List<String>  phoneWithCredential(BigDecimal actId);
    public  List<Integer>  overTimeItems(@Param(value = "actId") BigDecimal actId, @Param(value = "endDate") Date endDate);
    public  List<String>  getPhones(BigDecimal actId);

    public int queryTrophyCount(@Param("actId") BigDecimal actId, @Param("trophyId") BigDecimal trophyId);

    public List<TrophyItemCount> getTrophyItemCount(BigDecimal actId);

    public int getLotteryCount(@Param(value = "actId") Long actId, @Param(value = "credentialValue") String credentialValue);
    public List<TrophyItem> getTrophyItemByPhone(@Param(value = "actId") Long actId, @Param(value = "phone") String phone);
    public List<TrophyItem> getTrophyItemByCredentialValue(@Param(value = "actId") Long actId, @Param(value = "credentialValue") String credentialValue);

    List<TrophyItemInfo> querySharedRecords(@Param(value = "actId") Long actId,@Param(value = "credentialValue") String credentialValue);

    TrophyItemInfo querySharedRecordByTrophyId(@Param(value = "actId") Long actId,@Param(value = "trophyId") Long trophyId
            ,@Param(value = "credentialValue") String credentialValue);

    public int dropTableByName(String tableName);

}
