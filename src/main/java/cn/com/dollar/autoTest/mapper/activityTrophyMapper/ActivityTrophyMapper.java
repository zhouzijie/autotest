package cn.com.dollar.autoTest.mapper.activityTrophyMapper;

import cn.com.dollar.autoTest.model.ActivityTrophyMapperModel;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by ZZJ on 2016-12-2.
 */
public interface ActivityTrophyMapper {
    public List<ActivityTrophyMapperModel> getTrophyMessage(BigDecimal actId);

}
