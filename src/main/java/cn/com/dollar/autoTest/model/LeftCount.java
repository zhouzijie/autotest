package cn.com.dollar.autoTest.model;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-12-7.
 */
public class LeftCount {
    private BigDecimal trophyId;
    private String left;
    private String redisLeft;

    public BigDecimal getTrophyId() {
        return trophyId;
    }

    public void setTrophyId(BigDecimal trophyId) {
        this.trophyId = trophyId;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRedisLeft() {
        return redisLeft;
    }

    public void setRedisLeft(String redisLeft) {
        this.redisLeft = redisLeft;
    }
}
