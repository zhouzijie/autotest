package cn.com.dollar.autoTest.model;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-11-24.
 */
public class ShareCase {
    private BigDecimal shareCaseId;
    private BigDecimal trophyId;
    private BigDecimal actId;
    private String  shareBuffType;
    private int shareBuffValue;
    private String workCondition;
    private int workTimes;

    public BigDecimal getShareCaseId() {
        return shareCaseId;
    }

    public void setShareCaseId(BigDecimal shareCaseId) {
        this.shareCaseId = shareCaseId;
    }

    public BigDecimal getTrophyId() {
        return trophyId;
    }

    public void setTrophyId(BigDecimal trophyId) {
        this.trophyId = trophyId;
    }

    public BigDecimal getActId() {
        return actId;
    }

    public void setActId(BigDecimal actId) {
        this.actId = actId;
    }

    public String getShareBuffType() {
        return shareBuffType;
    }

    public void setShareBuffType(String shareBuffType) {
        this.shareBuffType = shareBuffType;
    }

    public int getShareBuffValue() {
        return shareBuffValue;
    }

    public void setShareBuffValue(int shareBuffValue) {
        this.shareBuffValue = shareBuffValue;
    }

    public String getWorkCondition() {
        return workCondition;
    }

    public void setWorkCondition(String workCondition) {
        this.workCondition = workCondition;
    }

    public int getWorkTimes() {
        return workTimes;
    }

    public void setWorkTimes(int workTimes) {
        this.workTimes = workTimes;
    }
}
