package cn.com.dollar.autoTest.model;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-12-2.
 */
public class ActivityTrophyMapperModel {
    private BigDecimal actId;
    private BigDecimal trophyId;
    private BigDecimal groupId;
    private String displayName;
    private int quantityLimit;
    private int weight;
    private int groupPriority;

    public BigDecimal getActId() {
        return actId;
    }

    public void setActId(BigDecimal actId) {
        this.actId = actId;
    }

    public BigDecimal getTrophyId() {
        return trophyId;
    }

    public void setTrophyId(BigDecimal trophyId) {
        this.trophyId = trophyId;
    }

    public BigDecimal getGroupId() {
        return groupId;
    }

    public void setGroupId(BigDecimal groupId) {
        this.groupId = groupId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getQuantityLimit() {
        return quantityLimit;
    }

    public void setQuantityLimit(int quantityLimit) {
        this.quantityLimit = quantityLimit;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getGroupPriority() {
        return groupPriority;
    }

    public void setGroupPriority(int groupPriority) {
        this.groupPriority = groupPriority;
    }
}
