package cn.com.dollar.autoTest.model;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2017-2-17.
 */
public class ProjectProperties {
    private BigDecimal ipWarningReqTimes;

    public BigDecimal getIpWarningReqTimes() {
        return ipWarningReqTimes;
    }

    public void setIpWarningReqTimes(BigDecimal ipWarningReqTimes) {
        this.ipWarningReqTimes = ipWarningReqTimes;
    }
}
