package cn.com.dollar.autoTest.model;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2016-12-7.
 */
public class TrophyItemCount {
    private int count;
    private BigDecimal trophyId;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getTrophyId() {
        return trophyId;
    }

    public void setTrophyId(BigDecimal trophyId) {
        this.trophyId = trophyId;
    }
}
