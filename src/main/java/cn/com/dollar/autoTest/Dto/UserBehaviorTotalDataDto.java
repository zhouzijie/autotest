package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.autoTest.util.Configs;
import cn.com.dollar.engine.statistics.model.type.StatisticsNumType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ZZJ on 2017-5-4.
 */
public class UserBehaviorTotalDataDto {
    private String actId;
    private Date startTime;
    private Date endTime;
    private StatisticsNumType dataType;


    public static List<UserBehaviorTotalDataDto> getList(Date startTime,Date endTime){
        StatisticsNumType types[]={StatisticsNumType.CLICK_AD_COUNT,StatisticsNumType.CLICK_AD_USER_COUNT,
                StatisticsNumType.ONLINE_LONGEST_TIME,StatisticsNumType.ONLINE_SUM_TIME,
                StatisticsNumType.DRAW_COUNT,StatisticsNumType.DRAW_USER_COUNT
//                ,StatisticsNumType.SHARE_COUNT,StatisticsNumType.SHARE_USER_COUNT
        };
        List<UserBehaviorTotalDataDto> list=new ArrayList<>();

        for(StatisticsNumType type:types){
            UserBehaviorTotalDataDto dto=new UserBehaviorTotalDataDto();
            dto.setActId(Configs.getPageViewAlias);
            dto.setDataType(type);
            dto.setStartTime(startTime);
            dto.setEndTime(endTime);
            list.add(dto);
        }

        return list;
    }
    public String getActId() {
        return actId;
    }

    public void setActId(String actId) {
        this.actId = actId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public StatisticsNumType getDataType() {
        return dataType;
    }

    public void setDataType(StatisticsNumType dataType) {
        this.dataType = dataType;
    }
}
