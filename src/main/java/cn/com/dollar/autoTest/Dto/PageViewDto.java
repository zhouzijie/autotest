package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.statistics.model.type.PlatformBrowserType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ZZJ on 2017-5-2.
 */

public class PageViewDto implements Serializable{
        private String openId;
        private String activityId;
        private String channelId;
        private String browserId;
        private Long stayTime;
        private PlatformBrowserType platformBrowserType;
        private List<ActionDto> actions;
        private String ipAddress;
        private String province;
        private String browserType;
        private String terminal;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getBrowserId() {
        return browserId;
    }

    public void setBrowserId(String browserId) {
        this.browserId = browserId;
    }

    public Long getStayTime() {
        return stayTime;
    }

    public void setStayTime(Long stayTime) {
        this.stayTime = stayTime;
    }

    public PlatformBrowserType getPlatformBrowserType() {
        return platformBrowserType;
    }

    public void setPlatformBrowserType(PlatformBrowserType platformBrowserType) {
        this.platformBrowserType = platformBrowserType;
    }

    public List<ActionDto> getActions() {
        return actions;
    }

    public void setActions(List<ActionDto> actions) {
        this.actions = actions;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getBrowserType() {
        return browserType;
    }

    public void setBrowserType(String browserType) {
        this.browserType = browserType;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
}
