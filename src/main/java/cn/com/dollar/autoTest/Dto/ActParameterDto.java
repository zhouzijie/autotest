package cn.com.dollar.autoTest.Dto;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2017-4-6.
 */
public class ActParameterDto {
    private Long actId;
    private BigDecimal lotteryDayTimes;
    private BigDecimal lotteryTotalTimes;
    private BigDecimal partDayTimes;
    private BigDecimal partTotalTimes;

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public BigDecimal getLotteryDayTimes() {
        return lotteryDayTimes;
    }

    public void setLotteryDayTimes(BigDecimal lotteryDayTimes) {
        this.lotteryDayTimes = lotteryDayTimes;
    }

    public BigDecimal getLotteryTotalTimes() {
        return lotteryTotalTimes;
    }

    public void setLotteryTotalTimes(BigDecimal lotteryTotalTimes) {
        this.lotteryTotalTimes = lotteryTotalTimes;
    }

    public BigDecimal getPartDayTimes() {
        return partDayTimes;
    }

    public void setPartDayTimes(BigDecimal partDayTimes) {
        this.partDayTimes = partDayTimes;
    }

    public BigDecimal getPartTotalTimes() {
        return partTotalTimes;
    }

    public void setPartTotalTimes(BigDecimal partTotalTimes) {
        this.partTotalTimes = partTotalTimes;
    }
}
