package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.act.share.type.ShareBuffType;
import cn.com.dollar.engine.act.share.type.WorkCondition;

import java.math.BigDecimal;

/**
 * Created by ZZJ on 2017-4-17.
 */
public class PortalShareCaseDto {
    private String trophyId;
    private Long actId;
    private ShareBuffType shareBuffType;
    private BigDecimal shareBuffValue;
    public Integer shareBuffTimes;
    private WorkCondition workCondition;
    private Integer workTimes;

    public String getTrophyId() {
        return trophyId;
    }

    public void setTrophyId(String trophyId) {
        this.trophyId = trophyId;
    }

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public ShareBuffType getShareBuffType() {
        return shareBuffType;
    }

    public void setShareBuffType(ShareBuffType shareBuffType) {
        this.shareBuffType = shareBuffType;
    }

    public BigDecimal getShareBuffValue() {
        return shareBuffValue;
    }

    public void setShareBuffValue(BigDecimal shareBuffValue) {
        this.shareBuffValue = shareBuffValue;
    }

    public Integer getShareBuffTimes() {
        return shareBuffTimes;
    }

    public void setShareBuffTimes(Integer shareBuffTimes) {
        this.shareBuffTimes = shareBuffTimes;
    }

    public WorkCondition getWorkCondition() {
        return workCondition;
    }

    public void setWorkCondition(WorkCondition workCondition) {
        this.workCondition = workCondition;
    }

    public Integer getWorkTimes() {
        return workTimes;
    }

    public void setWorkTimes(Integer workTimes) {
        this.workTimes = workTimes;
    }
}
