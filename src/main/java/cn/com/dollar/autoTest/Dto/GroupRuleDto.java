package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.trophy.main.model.type.LimitDepend;

import java.math.BigDecimal;

/**
 * Created by zyj on 2017/2/10.
 */

public class GroupRuleDto {
    private Long actId;
    private Long trophyId;
    private Long groupId;
    private BigDecimal weight;
    private BigDecimal quantityLimit;
    private LimitDepend limitDepend;

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public Long getTrophyId() {
        return trophyId;
    }

    public void setTrophyId(Long trophyId) {
        this.trophyId = trophyId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getQuantityLimit() {
        return quantityLimit;
    }

    public void setQuantityLimit(BigDecimal quantityLimit) {
        this.quantityLimit = quantityLimit;
    }

    public LimitDepend getLimitDepend() {
        return limitDepend;
    }

    public void setLimitDepend(LimitDepend limitDepend) {
        this.limitDepend = limitDepend;
    }
}
