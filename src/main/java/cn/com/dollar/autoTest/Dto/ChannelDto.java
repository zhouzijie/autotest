package cn.com.dollar.autoTest.Dto;

/**
 * Created by ZZJ on 2017-4-17.
 */
public class ChannelDto {
    private Long actId;
    private Long channelId;
    private String channelName;

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }
}
