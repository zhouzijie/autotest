package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.statistics.model.type.StatisticsNumType;

/**
 * Created by ZZJ on 2017-5-8.
 */
public class ActivityTotalStaQueryDto {
    private String actId;
    private StatisticsNumType dataType;

    public String getActId() {
        return actId;
    }

    public void setActId(String actId) {
        this.actId = actId;
    }

    public StatisticsNumType getDataType() {
        return dataType;
    }

    public void setDataType(StatisticsNumType dataType) {
        this.dataType = dataType;
    }
}
