package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.statistics.model.type.PageActionType;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by zyj on 2017/4/19.
 */

public class ActionDto implements Serializable{
    public PageActionType getAction() {
        return action;
    }

    public void setAction(PageActionType action) {
        this.action = action;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    private PageActionType action;
    private Date time;
    private String channel;
}
