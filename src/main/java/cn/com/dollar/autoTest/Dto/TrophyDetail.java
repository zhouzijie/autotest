package cn.com.dollar.autoTest.Dto;

/**
 * Created by ZZJ on 2017-4-17.
 */
public class TrophyDetail {
    private boolean address;
    private boolean idNo;
    private boolean phone;
    private boolean consumerName;
    private boolean bankCardId;
    private String agencyName;
    private String packetName;
    private String wishes;
    private String timeLimit;

    public static TrophyDetail getDefault(){
        TrophyDetail trophyDetail=new TrophyDetail();
        trophyDetail.setAddress(false);
        trophyDetail.setIdNo(false);
        trophyDetail.setPhone(false);
        trophyDetail.setConsumerName(false);
        trophyDetail.setBankCardId(false);
        trophyDetail.setAgencyName("");
        trophyDetail.setPacketName("");
        trophyDetail.setWishes("");
        trophyDetail.setTimeLimit("");
        return trophyDetail;
    }

    public boolean isAddress() {
        return address;
    }

    public void setAddress(boolean address) {
        this.address = address;
    }

    public boolean isIdNo() {
        return idNo;
    }

    public void setIdNo(boolean idNo) {
        this.idNo = idNo;
    }

    public boolean isPhone() {
        return phone;
    }

    public void setPhone(boolean phone) {
        this.phone = phone;
    }

    public boolean isConsumerName() {
        return consumerName;
    }

    public void setConsumerName(boolean consumerName) {
        this.consumerName = consumerName;
    }

    public boolean isBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(boolean bankCardId) {
        this.bankCardId = bankCardId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getPacketName() {
        return packetName;
    }

    public void setPacketName(String packetName) {
        this.packetName = packetName;
    }

    public String getWishes() {
        return wishes;
    }

    public void setWishes(String wishes) {
        this.wishes = wishes;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }
}
