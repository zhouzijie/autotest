package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.statistics.model.type.StatisticsNumType;

import java.util.Date;

/**
 * Created by ZZJ on 2017-5-8.
 */
public class UserBehaviorDataQueryDto {
    private String actId;
    private StatisticsNumType dataType;

    // 表示统计的时间密度,	单位为秒，目前支持的有 60(每分钟),600（每十分钟）,3600（每小时）,86400 （每日）四种时间跨度
    private int timeSpan;

    // 表示在指定时间密度下,从当前时间往前搜索，所需的统计数据的个数
    private int dataCount;

    private Date startTime;
    private Date endTime;


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(int timeSpan) {
        this.timeSpan = timeSpan;
    }

    public String getActId() {
        return actId;
    }

    public void setActId(String actId) {
        this.actId = actId;
    }

    public StatisticsNumType getDataType() {
        return dataType;
    }

    public void setDataType(StatisticsNumType dataType) {
        this.dataType = dataType;
    }

    public int getDataCount() {
        return dataCount;
    }

    public void setDataCount(int dataCount) {
        this.dataCount = dataCount;
    }

}
