package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.autoTest.enumeration.ActionType;
import cn.com.dollar.customer.label.person.model.type.CredentialType;

/**
 * Created by zyj on 2016/8/10.
 */
public class ShareCaseDto {
    private String activityId;
    private String authKey;
    private CredentialType type;
    private ActionType actionType = ActionType.SHARE_ACTION;

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public CredentialType getType() {
        return type;
    }

    public void setType(CredentialType type) {
        this.type = type;
    }

}
