package cn.com.dollar.autoTest.Dto;

import cn.com.dollar.engine.act.main.dto.CheckInfoJson;
import cn.com.dollar.engine.act.main.model.type.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ZZJ on 2017-4-6.
 */
public class ActivityMainDto {
    private Long actId;
    private String actName;
    private ActType actType;
    private ActStatus actStatus;
    private String actDesc;
    private BigDecimal budget;
    private Date startDate;
    private Date endDate;
    private Date publishDate;
    private Date stopDate;
    private Date recoverDate;
    private String creator;
    private String lastModifier;
    private String searchContent;
    private Integer pageNum;
    private Integer pageSize;
    private DeliverSwitch deliverSwitch;
    private ActPin actPin;
    private CheckInfoJson checkInfoJson;
    private Boolean autoWinningRate;
    private ArchiveStatus archiveStatus;
    private String companyId;

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName;
    }

    public ActType getActType() {
        return actType;
    }

    public void setActType(ActType actType) {
        this.actType = actType;
    }

    public ActStatus getActStatus() {
        return actStatus;
    }

    public void setActStatus(ActStatus actStatus) {
        this.actStatus = actStatus;
    }

    public String getActDesc() {
        return actDesc;
    }

    public void setActDesc(String actDesc) {
        this.actDesc = actDesc;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public Date getRecoverDate() {
        return recoverDate;
    }

    public void setRecoverDate(Date recoverDate) {
        this.recoverDate = recoverDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public DeliverSwitch getDeliverSwitch() {
        return deliverSwitch;
    }

    public void setDeliverSwitch(DeliverSwitch deliverSwitch) {
        this.deliverSwitch = deliverSwitch;
    }

    public ActPin getActPin() {
        return actPin;
    }

    public void setActPin(ActPin actPin) {
        this.actPin = actPin;
    }

    public CheckInfoJson getCheckInfoJson() {
        return checkInfoJson;
    }

    public void setCheckInfoJson(CheckInfoJson checkInfoJson) {
        this.checkInfoJson = checkInfoJson;
    }

    public Boolean getAutoWinningRate() {
        return autoWinningRate;
    }

    public void setAutoWinningRate(Boolean autoWinningRate) {
        this.autoWinningRate = autoWinningRate;
    }

    public ArchiveStatus getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(ArchiveStatus archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
