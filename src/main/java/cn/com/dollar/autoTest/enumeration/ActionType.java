package cn.com.dollar.autoTest.enumeration;

/**
 * Created by zyj on 2016/8/8.
 */
public enum ActionType {
    SHARE_ACTION ("分享"),//分享行为
    WETCH_TALKING_SHARE ("微信会话分享"),//微信会话分享
    WETCH_FRIEND_CIRCLE_SHARE ("微信朋友圈分享");//微信朋友圈分享

    String actionName;

    ActionType(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName(){
        return actionName;
    }
}
