package cn.com.dollar.autoTest.enumeration;

/**
 * Created by ZZJ on 2016-11-15.
 */
public enum ParameterKey {
    LOTTERY_DAY_TIMES,
    LOTTERY_TOTAL_TIMES,
    PART_DAY_TIMES,
    PART_TOTAL_TIMES
}
